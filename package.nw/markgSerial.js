/*
 Copyright 2013-2019 Mark Giebler

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.

 Author: Mark Giebler

 2013-07-14
 Chrome browser extension to monitor and control a GPS disciplined reference clock, Arduino and other serial devices.
 (Also works with nw.js, copy to package.nw directory)

 Install steps (if using an unpacked directory):
 Use Google's chrome browser.
 1. In Chrome's address bar enter; chrome://extensions and enable the "Developer Mode"
 2. click "Load unpacked extension..." button
 3. In the pop-up dialog; navigate to where the files are and choose the folder above it, and Click OK.
 4. To run it: in chrome://extensions/ select the "Launch" link for this  extension
 OR run it by selecting the "Apps" icon in Chrome's toolbar then select the app.
 Or on windows: All Programs->Chrome Apps-> This app...
 Or get the Chrome Launcher App for the windows task bar: https://chrome.google.com/webstore/launcher


 Install steps (if using a packed *.crx file)
 1. In Chrome's address bar enter; chrome://extensions    and enable the "Developer Mode"
 2. Drag and drop the .crx file to the Chrome browser
 3. In the dialog prompt, select "Add"
 4. To run it: selecting the "Apps" icon in Chrome's toolbar then select the app.
 5. to add desktop icons, select the "details" link for the app in the extension page, create icons from there.


 To uninstall the app:
 - right click the app and select "Remove from Chrome"  (or "Uninstall)
 - In the pop-up dialog select "Remove"

 How to make *.crx file (packed app):
 1. To make this a packed app, Go to chrome://extensions  and enable the "Developer Mode"
 2. select "Pack extension...", browse and select the App's directory,
 2a. If you are updating a previous build, select also the PEM file (private key)
 3. select Pack and follow on screen prompts.

 ***  nw.js  Alternative ***
 By 2018, Google will end support of Apps on all OS except Chrome OS.
 Alternative native JS app framework: http://nwjs.io/  http://nwjs.io/blog/chrome-apps-support/  https://www.npmjs.com/package/nwjs
 nw.js  no console log support in "normal" version, see this: https://github.com/nwjs/nw.js/issues/1629
 nw.js use SDK version to get console log and use remote debug: https://github.com/nwjs/nw.js/wiki/debugging-with-devtools


 -----------------------------------------------------------------------------------------
 Release notes History:
 v0.0.1-0.0.10 2013-07-14
 - added persistent storage for serial port settings and repeat mode.
 - basic index and options pages and js
 - put in some style.
 - top div with options and serial port selection.
 - fixed bitrate number getting saved as string.
 - tx & rx divs
 - open serial port and rx any data into rx div
 - auto scrolling of rx div
 - Auto resize Tx Input box on window resize
 - Hook in Send button to tx data out serial port with EOL added.
 - update open port if options are changed. (bitrate, data bits, etc)
 v0.1.1 2013-09
 - auto resize height of rxdata div on window resize.
 - send options: checkboxes to add newline:  <CR> <LF>
 v0.1.2
 - When onRxError() called, close the open port.
 v0.1.3
 - add MarkG icon files
 - added background color to all buttons
 - added quick TX buttons
 - implemented TX auto repeat of last string
 v0.1.4
 - fixed delay of 0 bug in TX auto repeat.
 - fixed rxData div resize error due to empty status div. Added "Ready!" to status div at start up.
 - tuned rx log delay.
 - added a "Refresh" item to port drop down that it will re-build the drop down. Will close any open port before doing refresh.
 v0.1.5 2013-11-24
 - added displayName of port object to the dropdown name.
 - tested opening /dev/ftdiRED directly on Linux and it works. So can have option to add custom dev names for ports via udev rules.
 - Added port count to refresh status report.
 v0.1.6  2013-11-25  update for future public release.
 - do not log auto repeat delay count if greater than 3. Only log reloading number and 2 and 1.
 - start adding storage for tx buttons and custom port path
 - added global flag to disable RX data logging.
 - added global flag to disable low level (trace) logging.
 - added Rx buffer size span element to rxdata area.
 "version": "0.2.0"  2016-12-26
 - add ability to save Rxdata buffer (div) to file. https://github.com/GoogleChrome/chrome-app-samples/tree/master/samples/filesystem-access
 - changed setRxData to append new string to end of textContent instead of rewriting the entire thing each time.
 - made null chars visible in logging.
 "version": "0.2.1"   2016-12-26
 - remove null chars from rx data before putting in rxdata.
 "version": "0.2.2"   2016-12-29
 - reformatted the Options.html page to make better use of the space.
 - removed repeatMode on/off from persistent storage.
 - on device disconnect, stop any active repeatMode.
 - added cts flow control on/off option on Options page.
 - added UI control to show ctrl chars, and to remove nulls.
 - Options: added config of udev paths.
 - Added different colors to the port dropdown types (custom udev paths vs standard)
 - Options: added tx quick button config
 - Added checkbox to clear txData input box after tx.
 - Persisted all main UI checkbox settings in Chrome storage.
 "version": "0.2.3" 2016-12-29
 - *removed* Tried to improve efficiency of updating rxData div.  See this: http://jsfiddle.net/dx7Dy/13/ and https://hacks.mozilla.org/2011/11/insertadjacenthtml-enables-faster-html-snippet-injection/
 - report the handshake lines on port open, and after rx and tx events.
 - Add RTS and DTR line control checkbox.
 - need to scroll rx every event even if no newline (with timeout delay so we don't thrashes on heavy input streams)
 - Add checkbox to enable/disable onRxError handler
 - Add 32 string tx history buffer.
 "version": "0.2.4" 2016-12-30
 - send immediate when character typed. Mostly working, but control chars only partial due to cut/paste, etc keys.
 "version": "0.2.5" 2017-01-02
 - handle backspaces - stops at newline.
 - Fixed bug where immediate send not sending CRLF
 "version": "1.2.6" 2017-01-03
 - comment clean up.
 - default onRxError handling to enabled.
 - fixed bug in custom tx button when deleting config, button name was not defaulted.
 - fixed onRxError handler to check connectionID vs open ID, If multiple instances of app open, errors are broadcast to all apps with open ports.
 "version": "1.2.7"
 - add flag to turn off console.log overriding.
 - kill unpause timer if port is closed before timer expires. Otherwise get a runtime error.
 - fix bug in unpause after rsError scheduling that fired unpause too soon.
 - made status area multi-line to handle multiple error messages in rapid order.
 - cleaned up console logging to be less chatty.
 - added 100mSec repeat Tx time selections.
 "version": "1.2.8"
 - added missing handling of "overrun" error in RxError handler.
 - Changed tx input box to textarea such that: cut and paste to input box retain EOL chars.
 - Cleaned up rxdata area resize method to properly handle variable size of objects above it
 - Fixed icon table layout so horizontal resize does not cause elements to overlap with lower status element.
 - added a default blank tx history string so that when using tx history you can blank out the tx box quickly.
 - clicking icon will increase log level by 1 for each click.
 - fixed bug: recalled history not being sent when in tx immediate mode. If enter key hit and history select is not -1, then send the tx input box text.
 - added 230400 bitrate
 "version": "1.2.9" 2017-01-08
 - allow rx scroll to happen without rescheduling timer if more than 24 EOL chars seen since last scroll.
 - trim back rx data area by 3% if size exceeds rxMaxBufferLength. Max set to 1M.
 - 1.2.9.1 - changed to outerBounds on window create.
 "version": "1.3.0.0" 2017-01-10
 - changed rxdata to a readonly textarea to allow text select and copy: My fiddle used to prototype: https://fiddle.jshell.net/wyq272b2/
 "version": "1.3.0.1" 2017-01-12
 - improved HW line status wrapping when app resized to very narrow.
 - bug fix: auto tx repeat was not stopped when port was closed.
 "version": "1.4.0.0" 2017-01-21
 - bug fix: on Linux; immediate send no longer grabbing the right data to tx. .key element not available in chrome on linux.
 - 1.4.0.1 added delete key to the makeCTRLvisible function.
 "version": "1.5.0.0"
 - Add option drop down to set max Rx display buffer size (slower or less memory systems need smaller max)
    1MB, 500KB, [250KB], 100KB, 50KB, 25KB, 10KB  - [] == start up default.
 "version": "1.5.0.1"
 - bug fix:  no scroll mode is scrolling to top of textarea on Linux.  Seems to have broke when we went to a textarea in v 1.3.0.0.
 "version": "1.6.0.1" 2017-01-26
 - versioning a bit haphazard up to now. Right most (4th digit) will now be bug fixes and minor tweaks to existing features.
 - add timestamp to received data (only per line so after first char after CR/LF is seen.) rxTimestamp
 "version": "1.6.0.2"
 - change timestamp to local time. Was UTC.
 "version": "1.6.0.3"
 - minor tweak of some status messages.
 - tx history recall now will put cursor at the end of the line not beginning.
 - Typing when Rx data area has focus will move focus to tx input box.
 - Linux socat virtual tty will fail on getting HW signal line status, added code to reduce number of errors logged.
    socat: https://justcheckingonall.wordpress.com/2009/06/09/howto-vsp-socat/
    I used this to test:  socat -d -d pty,raw,echo=1,echoctl=0 pty,raw,echo=1,echoctl=0
    It creates: /dev/pts/2  and  /dev/pts/3  which are looped together.
    This also works:  socat -d -d pty,raw,echo=0,link=/tmp/ttyV0 pty,raw,echo=0,link=/tmp/ttyV1
 "version": "1.6.1.3"
 - Add local echo option -  persisted to sync storage.
 - rxTimestamp flag now persisted to sync storage
 "version": "1.6.1.4"
 - Fix bug: tx history did not blank when end of history reached with MAX items in history buffer.
 "version": "1.6.2.0" 2017-04-25
 - Changed all storage from Global (sync) to local since serial port configs and usage is usually unique to local PC, not every PC you install this on.
 - Added local storing of port settings (BitRate, bits, etc)  for each port + description.
 - rxMaxBufferLength now persisted to local storage.
 "version": "1.6.2.1" 2017-05-11
 - Tested with nw.js running this instead of google Chrome. Works OK.
 - tweaked some log message.
 - tweaking onRX error recovery to try to unpause the port 20 times.  rxErrorUnpauseMax
 - Fixed default port value changing last port that was opened instead of the default port setting.
     However, if port was close automatically due to RX error, then changing port settings will update last port open
     This allows baud rate/data bits update if it was wrong when first opened.
     If you want to change default after such case, select "refresh" from list to force open port to None (Default)
 - Port list now refreshed automatically when saving options if no port is open.
 - added method to select and modify saved port settings directly in Options page.
 - added method to delete saved port settings in Options page.
 "version": "1.6.2.2"
 - add more control chars to show ctrl mode makeCTRLvisible(). Including chars with high bit set.
 "version": "1.6.2.3" 2017-05-20
 - add checkbox to show RxErrors in Rx Data window. Like <frame_error>  or <break>
 - tweaked onRxError handling more.
 - moved status div to bottom of UI.
 - resize app frame when status area changes in number of lines displayed to prevent
    over flow below bottom of app frame.
 - allow inputing HEX or BIN values using \xXX or \bXXXXXX in send box. Also \r \n \t \\  Only in non-immediateTx mode.
 "version": "1.6.2.4" 2017-05-26
 - clean up options page: shrink repeat mode area to one line.
 - added repeat count to repeat mode. (0 means forever)  (Saved to local storage.)
 - added ability to add more custom port fields to options page.  To delete: blank out path field, then save.
 - added checkbox to enable/disable the monitoring of the HW input lines.
 - moved log level bump up function to the green icon on the about.html page.
"version": "1.6.2.5" 2017-05-27
 - fixed bug that was not restoring the Show HW Lines setting from local storage.
"version": "1.6.2.6" 2017-06-03
 - try adding a flush button to call chrome api flush() to see if it fixes the nw.js dropped char issue on linux - no effect.  Hid button with style="display:none;"
 - (about.html) enhanced debug level setting green icon to allow increasing and decreasing log level.  Level 2 turns on Rx Data logging.
 - now allow additional quick send buttons to be added, 15 max. To delete buttons 6 thru 15, enter blank name on options.html
"version": "1.6.2.7" 2017-06-23
 - add port name to RX Error in status area when result is Port Closed.

"version": "1.6.2.8" 2017-08-08
 - disable auto repeat mode after cycle count reaches zero, otherwise it restart next time Options is entered.

"version": "1.6.2.9" 2017-08-21  solar eclipse day in the USA
 - increased tx history buffer to 64 strings, was 32.
 - add saving and restore of the tx history buffer via local storage. ID: txHistoryData
 - add checkbox to options.html to enable/disable tx history saving. Disabling will erase any saved history from local storage.

"version": "1.6.3.1" 2017-08-28
 - add text wrap checkbox to wrap  rxArea.  rxAutoWrap
 - add checkbox to hide / show tx quick buttons. txQuickHide
 - Changed color scheme of rxArea (greenish text on black)
 - Changed text color of buttons from white to greenish.
 - improved HW line status updating for reflecting changes sooner when looped back DTR/RTS signals are toggles.
 - Re-arranged Rx Data controls to make control wrap around less ugly.  (Really need to change to checkbox dropdowns)
 - expanded hex and binary input escape to accept \0x and \0b  (also fixed bug in handling \\\xHH cases.)

"version": "1.6.4.1" 2017-08-30
 - add ability to export settings to a JSON file.  (will add import feature later).
    Saves: custom port settings, quick-button settings, txHistory  (optionsStorageIDs, mainStorageIDs, txHistoryData)

"version": "1.6.5.1" 2017-08-31
 - export settings: close options page after save. otherwise options page is stuck behind main app.
 - add multi-checkbox drop downs to rx options area to contain previous options (timestamps, auto scroll, etc...) and add new HEX display of non-printable chars.

"version": "1.6.5.2" 2017-09-09
 - added 2.5K, 5K, and 2.5M options to RX scroll back buffer size selection. (storage.js)
 - Bug fix: rx size display not updating if continuous chars RX'ed. Changed to not reset timer on each char input, changed timer rate to 200mS.
 - Bug fix: rx display not auto scrolling if continuous data stream without EOLs received.
 - improved txHistory save.  It was saving when no data changed.

"version": "1.6.6.0" 2017-11-10
 - added ability to by pass EOL setting.  Use ctrl-ENTER to send LF only. Alt-ENTER to send CR only.  when in tx immediate mode..

"version": "1.6.7.0" 2017-11-10
 - fix the way normal and control chars are sent in txImmediate mode. Use String.fromCharCode(evt.charCode)
    To send ^C use SHIFT + CTRL + C (same for ^A, ^V and a few others)
 - Generate ANSI escape sequence for SHIFT + arrowKeys.
    SHIFT + key[Up]        = '^[[A'
    SHIFT + key[Down]      = '^[[B'
    SHIFT + key[Right]     = '^[[C'
    SHIFT + key[Left]      = '^[[D'

"version": "1.6.8.0" 2017-11-12
  - added backspace to display special character dropdown such that backspace can be enable separately from other non-printables.
  - added filter Bell (0x07) characters check item to Display Modes drop down
  - cleaned up display of EOL when \xXX or \0xXX format selected.
"version": "1.6.8.1"
  - bug fix with show Tab special chars item introduced when showBackspace was added.
"version": "1.6.9.0"
  - bug fix: History recall in txImmediate mode will result in messed up output if characters are appended before hitting return.
    temporarily disable (pause) tx immediate if history is recalled, then re-enable after Enter key or Send button hit.
  - Made function keys 1 - 10 aliases for quick send buttons 1 - 10
"version": "1.6.9.1"
  - Changed default font size for TX and RX areas to 10pt.  Was 12pt.
  - aligned send button to top of tx input box.

"version": "1.6.10.0"
  - add import of quick send button strings from json file (and  custom tty settings,txHistory,...)to options page  Export is already done.
  - add font size selection for changing the font in the  RX and TX data boxes.

"version": "1.6.10.1"
 - fix bug: import function did not update imported txHistory to local storage. Imported history was lost.
 - fix bug: txHistory recall of empty string will leave txImmediate mode disabled if it was paused previously.
 - enhanced txHistory; - don't store tx data in txHistory that is 1 character, or greater than 255 characters.

"version": "1.6.11.0"
 - add total TX and RX character counts to status line.
 - add display of current port settings to the right of the Go To Options button.
 - fix bug with unprintable character dropdown not initializing tab option correctly.

"version": "1.6.12.0"
 - add feature save/restore font size setting.
 - set focus to the txInput box on start up.
 - add ability to delete lines from history buffer.  Select line with arrow keys then press Alt-Del to delete.  (Now Ctrl-Del)
"version": "1.6.12.1" 2017-12-15
 - bug fix: font size would change back to default if app was resized
 - added the odd value font sizes between 8 and 16 points.
"version": "1.6.12.2"
 - Enhancement; made rx buffer clear button also clear the Tx and Rx character counts.
 - Changed history delete key combo from ALT-Del to Ctrl-Del  to avoid collision with default XFCE4 window manager setting using that key combo. (on Linux)
 - bug fix in history recall on down arrow; was not stopping at Head location. up arrow was not wrapping properly when circular buffer wrapped around.
 - feature allowing deleting of history items introduced a couple of bugs:
    - deleting history items when head and tail had wrapped around in the circular buffer corrupted the pointers.
    - history restored incorrectly from local store when head and tail wrapped around and items had been deleted.

"version": "1.6.13.0"
 - add 460800 & 921600 selections to baudrate list

"version": "1.6.13.1" 2018-01-14
 - bug fix: When timestamps are enabled, backspace handling doesn't stop at the end of the timestamp (which should be the new start of line location)

"version": "1.7.0.0" 2018-02-16
 - add feature: hex dump all received data:  XX XX XX XX XX  use auto wrap mode to align.

"version": "1.7.0.1" 2018-09-06
 - bug fix: With immediate Tx enabled, using tx history recall will send the next manually typed cmd twice; 'foofoo'.
 - enhancement: added one more log level (4) for RxData area logging.

"version": "1.7.1.0" 2019-03-20
 - added link to source code in about page.

"version": "1.7.2.0" 2019-04-01
 - enhancement to CTRL char input: now they send immediately regardless of tx immediate flag.
  e.g. SHFT-CTRL-C  will send Ctrl-C immediately even if send immediate is not on.

"version": "1.8.0.0" 2019-04-01
 - added color theme dropdown to rxdata area.

"version": "1.8.1.0" 2019-04-01
 - fixed git merge conflict

"version": "1.8.2.0"
 - enhancement for CTRL char input; escape key added as ctrl char so it will send immediately regardless of tx immediate flag.
 - enhancement handle escape and Fn keys when focus is in rxDataArea box

"version": "1.9.0.0" 2019-05-02
 - added tab width control
 - moved font selector to the quick button box to group with Theme selector and new tab width selector
 - made end of line characters in the source files consistent.

"version": "1.10.0.0" 2019-07-25
 - Added send text file feature with XON/XOFF support and line delay support (settable in Options page).
	- info.  https://github.com/GoogleChrome/chrome-app-samples/tree/master/samples/filesystem-access

"version": "1.10.0.1" 2019-07-02
 - Minor tweaks to file transfer feature;
 - Added window resize call to fix status line being cut off when file transfer starts.
 - Added file transfer status on complete or abort to status line.
 - Turned off status update during line by line transfer.

"version": "1.10.0.2" 2019-08-30
 - Added "Blueprint" theme.

"version": "1.10.0.3" 2019-0918
 - Minor addition to file upload feature: abort upload if ESC character sent by device.

"version": "1.10.1.3" 2020-02-05
 - work around: even with Receive Error Handling unchecked, the onRxError handler was still being called (How? It should not be registered with API)
   added test in onRxError handler to test check box unchecked, and not stop repeat TX timer if framing error, overrun error.

"version": "1.11.1.4" 2022-09-22
 - Add Auto Port Reopen feature to options. When checked, we will attempt to reopen the port if it was unexpectedly closed.
	This is helpful under Arduino CLI/IDE use where the port gets "stolen" out from under us.
 - Minor addition to file upload feature:  re-send of last line sent if NAK received during file sending.
 - Minor addition to file upload feature:  display average transfer rate during file sending. 

"version": "1.11.2.0" 2023-02-03
 - Modify index.html to swap order of Flow Control and Bps in file transfer area.
	This eliminates the Bps value from jittering around when XON/XOFF toggles.
 - Fix bug where BPS value could start out much higher than real if a second file tranfer was started.
	Side effect of this fix: the Tx total count at the bottom of the screen gets reset to zero at the
	start of each file transfer. (totalCharsXmtd set to zero.)

"version": "1.11.3.0" 2023-03-13
 - Fix issue #2. "Using nwjs-sdk-v0.73.0-linux-x64 results in invalid TTY ports to be listed."

"version": "1.11.4.0" 2023-04-12
 - Fix issue #3. "Using nwjs-sdk-v0.73.0-linux-x64 results in Send File not working."
 - Additional fix for issue #2 seen on older njws (0.54) on Ubuntu 20.04.

"version": "1.11.5.0"
 - Fixed NAK handling that caused NAK state to be lost.
 - Refactored file transfer logging to provide more trace data.

"version": "1.11.5.1"
 - Enhanced file transfer logging

"version": "1.11.6.0"
 - Finalise implementing correct ACK/NAK file transfer handshake method.

 Bugs:
 - Sustained high data rate reception causes the app to become extremely lethargic - near lock up.  Need to handle this some how. Check code around line 1077.
 - Auto Repeat Delay text does not show correctly for repeat delays less than 1 second.
 - (with two apps open) on Windows: when a long data stream is sent to our app  the receiving app gets a chrome API system error at around 4097 bytes received. (even if buffer size set larger.)
   sending side seems OK since entire string gets sent when a different terminal program is used to RX the data.
   With one App open, rx is OK with large data streams (110-115200 bps).  Seems only an issue if two apps are open simultaneously
 - (issue when pasting large chunks) queuing up multiple chrome api send calls results in the send data for second, third etc calls to be corrupted.
   apparently we need to wait until the send triggers our callback. Might need a queuing mechanism if busy sending.
 - (maybe this is OK.) pasting into tx box with immediate tx enabled; does not add string to tx history unless ENTER key is pressed in the tx box.
 - nw.js on old E3400 laptop on Linux when running at 115200 bps, drops characters and tx seem behind one or two tx send events, or input is being delayed. Decreasing rx buffer size helps this out.

 Todo:
 * Move to Electron framework:  https://www.electronjs.org/docs/tutorial/first-app
	packaging: https://github.com/Lyrillind/vrrv-builder  or  seems nicer: https://github.com/electron-userland/electron-builder
	Start app template: https://github.com/electron-userland/electron-webpack-quick-start  and  https://www.electron.build/#boilerplates
	Example project using electron-builder:  https://github.com/julian-alarcon/prospect-mail
	Or maybe QT + Python: https://build-system.fman.io/
 - add Close Port button in the port box to quickly close port under high data rate condition (see bug about high data rate)
 - add 1800 second to repeat count drop down - longer term make it an edit box.
 - add Shift-ENTER sends tx string without any EOL (Basically: overrides EOL settings)
 - Add custom bit rates for serial ports that support it. add to serialRateSelection in storage.js
 - add option to display data as HEX dump or BIN dump. Put in space char every 8 hex/bin digits - enable auto wrap? Honor CRLF? Hexdump is DONE.
 - setting to interpret tx input box as HEX or BIN without needing escapes: e.g "DEADBEEF" or "11010010"  ignore any other chars.
 - Support Nordic UART BLE using node.js noble: https://github.com/noble/noble
 - if port path has /pts/ in its name, then disable HW signal status right away.
  - rcv options: radio buttons newline on <CR> only to add  <LF> to readbuffer. Or use TX checkboxes?
 - handle CR only (CR is ignored by browser, LF is treated as CRLF by browser) CR will need to search backwards for last LF and save that location then on non-CR/LF overwrite at saved location.
 - improve efficiency of updating rxData div. Works, but CR char is lost when saving a file. See this: http://jsfiddle.net/dx7Dy/13/
	also see this for circular buffer to use for input buffering https://codepen.io/bave8782/pen/dXKvrW?editors=1111  then only update graphic elements from this buffer on a timer.
	Another circular buffer example which seems closer to what we need: http://blog.trevnorris.com/2012/03/javascript-circular-buffers.html and this https://yomguithereal.github.io/mnemonist/circular-buffer.html
	and this one could replace our history buffer (OOP) https://stackoverflow.com/a/28038535
 - add blinking cursor?  http://codepen.io/ArtemGordinsky/pen/GnLBq

 */

var logRxData = false;     // use to control if rx data is logged to console log. Enable at log level 4.
var logDebugLevel = 0;    // Level: 0==normal,  1==debug, 2==trace, 3==Insane, 4==logRxData
function getMaxLogLevel(){
    return 4;
}
function getLogLevel(){
    return logDebugLevel;
}
function increaseLogLevel(){
    if (logDebugLevel < getMaxLogLevel())
        logDebugLevel++;
    if (logDebugLevel ===getMaxLogLevel()) logRxData=true;
    console.log("++ logDebugLevel: " + logDebugLevel +"  logRxData: "+logRxData);
}
function decreaseLogLevel(){
    if (logDebugLevel > 0)
        logDebugLevel--;
    if (logDebugLevel < getMaxLogLevel()) logRxData=false;
    console.log("-- logDebugLevel: " + logDebugLevel +"  logRxData: "+logRxData);
}
// --------------------------------------
// console.log:
// Over load the console.log mechanism to put in
//  timestamps that can be saved with the  log file.
//  Stupid Chrome api doesn't allow one to save log files with the timestamps. We have to write our own logger to get savable timestamps.
//
// from: http://stackoverflow.com/questions/12008120/console-log-timestamps-in-chrome
//
// You will lose the source line number in the logging. It will always be at the line marked with ###@$ below
// This declaration needs to be at the top of the source file before any console.log() calls
// --------------------------------------
var overrideLogging = false;   // set to true to get timestamps that save to a file.
if (overrideLogging) {
    console.logCopy = console.log.bind(console);
    console.log = function ()
    {
        // format string for the timestamp
        var tsFormat = 'color:red';
        var defFormat = 'color:black';

        // Timestamp to prepend
        /*    var timestamp = new Date().toISOString();	// e.g. 2016-12-09T19:04:50.695Z
         timestamp=timestamp.replace("T" , " ");
         timestamp=timestamp.replace(/Z/ , " ");
         */
        var timestamp = getTimestampString(true);// get local time in ISO format.

        if (arguments.length) {
            // True array copy so we can call .splice()
            var args = Array.prototype.slice.call(arguments, 0);

            // If there is a format string then... it must
            // be a string
            if (typeof arguments[0] === "string") {
                // Prepend timestamp to the (possibly format) string
                args[0] = "%c%s%c :: " + arguments[0];

                // Insert the default format string where it has to be
                args.splice(1, 0, defFormat);
                // Insert the timestamp where it has to be
                args.splice(1, 0, timestamp);
                // Insert the tiemstamp format string where it has to be
                args.splice(1, 0, tsFormat);

                // Log the whole array
                this.logCopy.apply(this, args);		//  ###@$  this is the line number used for all logging ... ARGH!!
            } else {
                // "Normal" log
                this.logCopy(timestamp, args);		//  ###@$  this is the line number used for all logging ... ARGH!!
            }
        }
    };
}
// -----------------------------------------------------------------------------
//  Global variables
//  ----------------------------------------------------------------------------
// get app name and version number from our manifest.json file.
var manifest = chrome.runtime.getManifest();
console.log('from manifest.json: ' + manifest.name);
console.log('from manifest.json: ' + manifest.version);

var MyVersion = manifest.version;
var MyFileName = "markgSerial.js";

var portOptions = {// these names must match the Chrome API usage.
//	persistent:false,
    name: "None (Close port)", // Unique name of port
    bufferSize: 4096,
    bitrate: 9600,
    dataBits: "eight",
    parityBit: "no",
    stopBits: "one",
    ctsFlowControl: false
};
function getPortOptions() {
    return portOptions;
}

var openPortUniqueName = "None (Close port)";   // used to index portSettngsArray, set to default name.
var connectionId = -1;
var connectionError = false;    // gets set True by onRxError, reset by unpause OK, or onOpen. Will stop getHWsignals() when True
var unpauseCount = 0;           // count how many unpause attempts are made for RX errors.
var rxErrorLastError = '';      // keep track if same error twice in a row.
var autoPortReopen = false;		// if port closes unexpectedly, try to reopen the port every 1 second or until "Close Port" is selected.

var readBuffer = "";
var readLogBuffer = '';
var lineBuffer = "";
var rxLastBufferLength = 0;
var rxMaxBufferLength = 100000; // trim back by 3% when limit reached.
var rxBackspaceEnable = true;   // todo: add a UI checkbox to control this?
var rxInsertionIdx = 0;         // backspace chars will adjust this
var rxLastEolIdx = 0;
var rxEnableOnReceiveError = true;  // on Windows, the OnReceiveError API is a cluster f*ck for FTDI USB serial ports!
var rxErrorUnpauseMax = 20;     // maximum times in a row to try and unpause after Rx Error event.

var autoPortReopenTimer = null;	// used to time port reopen attempts.
var rxUnpauseTimer = null;
var rxScrollTimer = null;       // used to delay scrolling the rx window to reduce browser update thrashing.
var rxLogTimer = null;          // used to delay logging of rx data to cut down on log contents
var rxFilterNulls = true;       // remove null chars before storing in rxData area.
var rxFilterBells = true;       // remove bell chars before storing in rxData area.

var rxShowCtrls = false;        // show ctrl chars in rx area. <XX>
var rxShowCrtlHex = false;      // show ctrl chars as \xXX
var rxShowCtrlHex0 = false;     // show ctrl chars as \0xXX
var rxShowEOLrl = false;        // show eol as \r\n
var rxShowEOLesc = false;       // show eol as <CR>
var rxShowEOLhex = false;       // show eol as \x0A or \0x0A depending on ctrl Hex setting.
var rxShowTab = false;          // show tab chars to match Ctrl type or EOL \r \n type.
var rxShowBackspace = false;    // show backspace to match Ctrl type or EOL \r \n type.

var rxAllHexDump = false;       // show all characters as hex XX

var rxShowTimestamp = false;
var rxEnableShowHWLines = true;

var txRepeatDelay = 0;          // seconds to delay before resending last msg.
var txRepeatCount = 0;          // number of times to repeat, 0 means forever.
var txRepeatMode = false;

var customPorts;                // will hold an array of ports read from storage.
var customTxButtons;            // an array of custom buttons from storage

// get elements once to save some cycles
var rxDataElement = document.getElementById('rxdata');
// get window height and width objects. Used to resize tx and rx boxes.
var heightOutput = document.querySelector('#height');
var widthOutput = document.querySelector('#width');
var txInputElement = document.querySelector('#txdata');

var totalCharsRcvd = 0;         // total characters received since app started.
var totalCharsXmtd = 0;         // running total of characters transmitted for active file transfer.

var txLineDelay = 100;			// mSec to delay after sending a line from a file.
var txLinesToSend = 0;			// total number of lines to send, set to zero to cancel an in progress send.
var txLinesTotal = 0;			// saved copy of total lines to send, for logging use.
var lineToSend = 0;				// current line that will be sent.
var txLinesPaused = false;		// set to true to pause file transfer, e.g. XOFF received.
var stateLineByLine = 8;		// 0 - ready to send line, 1 - waiting for XOFF, 2 - waiting for ACK/XON, 3 - rcvd XON, 4 - rcvd NAK waiting for XON, 5 - ready to RE-send line, 6 - rcvd ACK waiting for XON, 8 - waiting first handshake, 9 - Aborted
var txFileTimeStart;			// Time a file transfer started. Used for Bytes/sec calculation.
var pausedLogTime;				// Determining how long file transfer was paused.
var ackNakMode = false;		// true if using ACK/NAK handshake in file transfer mode. Determined by getting NAK while waiting for XON on first line.

// set max rx buffer size
function setMaxRxBufferLength(rxLength)
{
    rxMaxBufferLength = rxLength;
    console.log('setMaxRxBufferLength: ' + rxMaxBufferLength);
}
function getMaxRxBufferLength()
{
    console.log('getMaxRxBufferLength: ' + rxMaxBufferLength);
    return rxMaxBufferLength;
}
setMaxRxBufferLength(250000);
//
// string to arrayBuffer conversion.
function str2ab(str) {
    var buf = new ArrayBuffer(str.length); // 1 byte for each char using Uint8Array instead of Uint16Array.
    var bufView = new Uint8Array(buf);
    for (var i = 0, strLen = str.length; i < strLen; i++) {
        bufView[i] = str.charCodeAt(i);
    }
    return buf;
}

// arrayBuffer to string conversion
function ab2str(buf) {
    return String.fromCharCode.apply(null, new Uint8Array(buf));
}

var statusElement = document.getElementById('status');
var statusMultiLine = false;
//function setStatus(status) usage: setStatus(statusStr,'red')  or setStatus(statusStr,'#ff0000')
// set the status display area with some text and optional color.
// If \n or \t  at start of line, then append to existing logging.
function setStatus(status, colorCode) {
    if (logDebugLevel >= 2)
        console.log("setStatus: [" + status + "] ", colorCode);

    if (status.charAt(0) === '\n' || status.charAt(0) === '\t' ) {
        statusElement.textContent += status;
        if (status.charAt(0) === '\n'){
            resizeThrottler();  // need to resize window when multiple lines added to status div.
            statusMultiLine = true;
        }
    } else {
        statusElement.textContent = status;
        if (statusMultiLine) {
            resizeThrottler();  // need to shrink window when multiple lines added previously status div.
            statusMultiLine = false;
        }
    }
    if (!colorCode) {
        statusElement.style.color = 'black'; // no color passed in, default to black
        statusElement.style.fontWeight = 'normal';
    } else {
        statusElement.style.color = colorCode; // red or #ff0000, orange, etc
        statusElement.style.fontWeight = 'bold';
    }
}

getStatus = function () {
    return document.getElementById('status').textContent;
};
function updateHWstatus(status)
{
    // status is an optional string. If not given, then we call getHWsignals() to get latest info.
    // update output signals each time before we read input signals (in case outputs are looped back to inputs)
    if (connectionId !== -1)
        chrome.serial.setControlSignals(connectionId, {dtr: txDTR, rts: txRTS}, function () {
            if (chrome.runtime.lastError)
            {
                hwStatusFailCount++;
                console.error('updateHWstatus(): setControlSignals chrome API lastError:', chrome.runtime.lastError.message);
                setStatus('\nChrome API: ' + chrome.runtime.lastError.message, 'red');
            }
        });
    if (status) {
        var statusElement = document.getElementById('linestatus');
        statusElement.innerHTML = status;
    } else {
        var hwUpdateTimer = window.setTimeout(function () {
            // we delay 66mSec to give serial API time to update line status.
            getHWsignals();// this will call us back with a status string
            hwUpdateTimer = null;
        }, 66);
    }
}

// my fiddle: https://fiddle.jshell.net/9ue3n807/ & 1/
// return a time stamp string in near ISO format in
// either local time or UTC
function getTimestampString(localTime) {
    var utcMs = new Date().getTime();
    var tzOffMs = 0;
    if (localTime)
        tzOffMs = new Date().getTimezoneOffset() * 60 * 1000;
    var localTimeMs = utcMs - tzOffMs;
    var timestamp = new Date(localTimeMs).toISOString();
    timestamp = timestamp.replace("T", " ");// remove annoying T & Z
    timestamp = timestamp.replace(/Z/, " ");
    return timestamp;
}
// convert2Hex()
// Will convert character at string[offset] to hex
// and return a string with two hex digits.
function convert2Hex(offset, string){
    var num = 0;
    num = string.charCodeAt(offset);
    var hex = num.toString(16).toUpperCase();
    if (num < 16)
        hex = "0" + hex;
    return hex;
}
// escape2Hex()  used with string.replace()
//  therefore the names, order and number of parameters must be as they are.
//          match is unused, but must be there.
function escape2Hex(match, offset, string) {
    var num = 0;
    num = string.charCodeAt(offset);
    var hex = num.toString(16).toUpperCase();
    if (num < 16)
        hex = "0" + hex;
    return addEscapeDecoration(hex);
}
function addEscapeDecoration(hex){
    // take a string assumed to be two hex digits and put the escape chars
    // in place.  Use the escape chars per the mode we are in.
    // such that input string "A5" becomes <A5> or /0xA5 depending on mode.

    //  add more modes : /x /0x
    if(rxShowCtrls)
        return "<" + hex + ">";
    if(rxShowCrtlHex)
        return "\\x" + hex;
    // assume (rxShowCtrlHex0)
        return "\\0x" + hex;
}
// make non-printable chars visible based on settings:
// rxShowCtrls || rxShowCrtlHex || rxShowCtrlHex0 || rxShowEOLrl || rxShowEOLesc || rxShowEOLhex || rxShowTab || rxShowBackspace
//
function makeCTRLvisible(str) {
    // handle backspace specially
    if(rxShowBackspace && rxShowEOLrl){
        str = str.replace(/\x08/g, "\\b");
    }else if(rxShowBackspace && rxShowCtrls){
        str = str.replace(/\x08/g, "<BS>");
    }else if(rxShowBackspace){
        str = str.replace(/\x08/g, escape2Hex);
    }
    // handle tab specially
    if(rxShowTab && rxShowEOLrl){
        str = str.replace(/\x09/g, "\\t");
    }else if(rxShowTab && rxShowCtrls){
        str = str.replace(/\x09/g, "<HT>"); // 1.6.2.2
    }else if(rxShowTab){
        str = str.replace(/\x09/g, escape2Hex);
    }
    if(rxShowCtrls){
        // as mnemonics
        str = str.replace(/\x00/g , "<Nul>");
        str = str.replace(/\x01/g , "<SOH>"); // SOH
        str = str.replace(/\x02/g , "<STX>"); // ^b
        str = str.replace(/\x03/g , "<ETX>"); // ^c
        str = str.replace(/\x04/g , "<EOT>"); // ^d

        str = str.replace(/\x05/g , "<ENQ>"); // ^e
        str = str.replace(/\x06/g, "<ACK>"); // 1.6.2.2
        str = str.replace(/\x15/g, "<NAK>"); // 1.6.2.2

        str = str.replace(/\x07/g, "<BeL>"); // 1.6.2.2

        str = str.replace(/\x0b/g, "<VT>"); // 1.6.2.2
        str = str.replace(/\x0c/g, "<0C>"); // 1.6.2.2

        str = str.replace(/\x11/g , "<dc1>"); // ^q XON
        str = str.replace(/\x12/g , "<dc2>"); // ^r
        str = str.replace(/\x13/g , "<dc3>"); // ^s XOFF
        str = str.replace(/\x14/g , "<dc4>"); // ^t

        str = str.replace(/\x1b/g, "<ESC>");
        str = str.replace(/\x7f/g, "<DEL>"); // delete key
    }
    if(rxShowCtrls || rxShowCrtlHex || rxShowCtrlHex0){
        // 1.6.2.2  Make more ctrl and all high bit set chars visible.
        //    var str2 = String.fromCharCode(0x81);str += str2; // debug testing
        str = str.replace(/[\x00-\x07]|[\x0e-\x1f]/g, escape2Hex); // skip CR to LF

        // all high bit chars. And include DEL 0x7F character
        str = str.replace(/[\x7F-\xFF]/g, escape2Hex);
    }
    // EOL handling last
    if(rxShowEOLesc ){
        str = str.replace(/\x0a/g, "<LF>\x0a");
        str = str.replace(/\x0d/g, "<CR>\x0d");
        // clean up eol
        str = str.replace(/<CR>\x0d<LF>\x0a/g, "<CR><LF>\x0d\x0a");
        str = str.replace(/<LF>\x0a<CR>\x0d/g, "<LF><CR>\x0a\x0d");
    }else if(rxShowEOLrl){
        str = str.replace(/\x0a/g, "\\n\x0a");
        str = str.replace(/\x0d/g, "\\r\x0d");
        // clean up eol
        str = str.replace(/\\r\x0d\\n\x0a/g, "\\r\\n\x0d\x0a");
        str = str.replace(/\\n\x0a\\r\x0d/g, "\\n\\r\x0a\x0d");
    }else if(rxShowEOLhex){
        //  hex replacement...
        str = str.replace(/\x0a|\x0d/g, escape2Hex);
        // clean up eol
        if(rxShowCtrls){
            str = str.replace(/<0A>/g, "<0A>\x0a");
            str = str.replace(/<0D>/g, "<0D>\x0d");
            // final clean up eol
            str = str.replace(/<0D>\x0d<0A>\x0a/g, "<0D><0A>\x0d\x0a");
            str = str.replace(/<0A>\x0a<0D>\x0d/g, "<0A><0D>\x0a\x0d");
        }else if(rxShowCrtlHex){
            str = str.replace(/\\x0D/g, "\\x0D\x0d");
            str = str.replace(/\\x0A/g, "\\x0A\x0a");
            // final clean
            str = str.replace(/\\x0D\x0d\\x0A\x0a/g, "\\x0D\\x0A\x0d\x0a");
            str = str.replace(/\\x0A\x0a\\x0D\x0d/g, "\\x0A\\x0D\x0a\x0d");
        }else if(rxShowCtrlHex0)
            str = str.replace(/\\0x0D/g, "\\0x0D\x0d");
            str = str.replace(/\\0x0A/g, "\\0x0A\x0a");
            // final clean
            str = str.replace(/\\0x0D\x0d\\0x0A\x0a/g, "\\0x0D\\0x0A\x0d\x0a");
            str = str.replace(/\\0x0A\x0a\\0x0D\x0d/g, "\\0x0A\\0x0D\x0a\x0d");
    }
    return str;
}

function rxEnableOnReceiveErrorOnChange(element)
{
    rxEnableOnReceiveError = element.checked;
    console.log('rxEnableOnReceiveErrorOnChange: ', rxEnableOnReceiveError, element);
    chrome.storage.local.set({rxEnableOnReceiveError: rxEnableOnReceiveError}, function () {
        ;
    });
    if (rxEnableOnReceiveError)
        chrome.serial.onReceiveError.addListener(onRxError);
    else
        chrome.serial.onReceiveError.removeListener(onRxError);
    // side effect: clear the status div:
    setStatus(' ');
}

function rxEnableShowReceiveErrorOnChange(element)
{
    // store new state in local storage
    console.log('rxEnableShowReceiveError: ', element.checked, element);
    chrome.storage.local.set({rxEnableShowReceiveError: element.checked}, function () {
        ;
    });
    // side effect: clear the status div:
    setStatus(' ');
}

var rxAutoWrap = true;
var rxAutoScroll = true;
var rxScrollEolCount = 0;   // used for allowing scroll to happen after so many EOLs.
var rxScrollCharCount = 0;  // for counting chars in a stream when no EOLs are coming in.
// when checkbox is toggled change the scroll setting
function rxAutoScrollOnChange(element)
{
    rxAutoScroll = element.checked;
    console.log('rxAutoScrollOnChange: ', rxAutoScroll, element);
    chrome.storage.local.set({rxAutoScroll: rxAutoScroll}, function () {
        ;
    });
}
// when checkbox is toggled change the wrap setting
function rxAutoWrapOnChange(element)
{
    rxAutoWrap = element.checked;
    console.log('rxAutoWrapOnChange: ', rxAutoWrap, element);
    chrome.storage.local.set({rxAutoWrap: rxAutoWrap}, function () {
        ;
    });
    rxAreaAppyWrapMode();
}

function rxAreaAppyWrapMode(){
    console.log('rxAutoWrap style: ',rxDataElement.style.whiteSpace);
    if(rxAutoWrap)
    {
        rxDataElement.style.whiteSpace = "pre-wrap";
    } else {
        rxDataElement.style.whiteSpace = "pre";
    }
}

function escapeHtml(text) {
    return text.replace(/[&<>"']/g, function (m) {
        switch (m) {
            case '&':
                return '&amp;';
            case '<':
                return '&lt;';
            case '>':
                return '&gt;';
            case '"':
                return '&quot;';
            default:
                return '&#039;';
            }
    });
}
// --------------------------------------------
// setRxData()
// This method takes the raw input bytes and
// displays them in the rx area based on various settings.
// This method handles NAK, XON, XOFF during file transfers.
// This method also handles backspace processing.
// Note: when rxAllHexDump is true, then only
// translation to hex is performed, no other processing (no filtering, no backspace handling, etc)
var rxDataLogDelay = null;
var rxSizeTimer = null;
var rxTimestampArmed = false;
function setRxData(data) {
    var scrollDelay = 330;
    var rxCopyStartIdx = 0;   // set to non-zero if rx buffer exceeds max.
    if (rxLastBufferLength > rxMaxBufferLength)
        rxCopyStartIdx = rxLastBufferLength / 32;  // window buffer overflow, trim rx data back by 3.125% of max.
    // save current scroll location to fix Linux scrolling when it should not.
    var topScrollSave = rxDataElement.scrollTop;
    if(rxAllHexDump){
        // convert all bytes received to HEX, display as " XX XX XX ..."
        var byteCount = data.length;
        var byteIndex = 0;
        var hexString="";
        while(byteCount){
           hexString += convert2Hex(byteIndex++, data);
           hexString += " ";
           byteCount--;
        }
        //rxDataElement.insertAdjacentHTML("beforeend", escapeHtml(hexString)); // v 0.2.3  this works, but results in CR characters getting striped out when saving file..
        rxDataElement.textContent = rxDataElement.textContent.substring(rxCopyStartIdx, rxInsertionIdx) +hexString;  // comment out if using above line.
        rxInsertionIdx = rxLastBufferLength = rxDataElement.textContent.length;

    }else{
		// if file transfer active, check for ACK, NAK, ESC, XOFF or XON and set file transfer pause accordingly
		// using indexOf() in the case the date buffer has multiple chars, we need to know which one was LAST
		if( txLinesToSend > 0 )	// Xfr active
		{
			if(logDebugLevel >= 1)
				console.log("setRxData() data.len: " + data.length);
			var xon  = data.indexOf("\x11");
			var xoff = data.indexOf("\x13");
			var esc  = data.indexOf("\x1b");
			var nak  = data.indexOf("\x15");
			var ack  = data.indexOf("\x06");
				// filter XON/XOFF/NAK
			data = data.replace(/\x11/g, '');
			data = data.replace(/\x13/g, '');
			data = data.replace(/\x15/g, '');
			data = data.replace(/\x06/g, '');

			if( nak > xoff )
			{
				console.log('NAK: ' + nak + '  xon: ' + xon + ' xoff: '+ xoff + "  StateLBL: " + stateLineByLine + " -> 4     ********");
				// determine if this transfer will use full ACK/NAK mode
				if( stateLineByLine === 8)
				{
					// if this is NAK while waiting first handshake
					ackNakMode = true;
				}
				stateLineByLine = 4;	// received NAK while waiting for sequence XOFF -> XON
				txLinesPaused = true;	// Treat like XOFF
				updateXOFFstatus(makeSpanColor('NAK','#911'));
				data = data.replace(/\x15/g, '');
			}else if( ack > xoff && ack > nak)
			{
				// determine if this transfer will use full ACK/NAK mode
				if( stateLineByLine === 8)
				{
					// if this is ACK while waiting first handshake
					ackNakMode = true;
				}
				if(ackNakMode){
					console.log('ACK: ' + ack + '  nak: ' + nak + ' xon: ' + xon + ' xoff: '+ xoff + "  StateLBL: " + stateLineByLine + " -> 6");
					stateLineByLine = 6;	// received ACK now waiting for XON.
					updateXOFFstatus(makeSpanColor('ACK','#191'));
				}else{
					console.log('ACK: ' + ack + '  nak: ' + nak + ' xon: ' + xon + ' xoff: '+ xoff + "  StateLBL: " + stateLineByLine + "  ack-IGNORED!");
				}
			}
			
			if( esc > xon || esc > xoff )
			{
				// receiver of data requests to abort.
				console.log('ESC: ' + esc +  '  xon: ' + xon + ' xoff: '+ xoff + ' ack: ' + ack + ' nak: ' + nak + "  StateLBL: " + stateLineByLine);
				txLinesToSend=0;
				txLinesPaused=false;
				stateLineByLine = 9;	// indicate abort
				updateXOFFstatus(makeSpanColor('XOFF','#911'));
			}else if( xon > xoff && (xon > nak || xon > ack))
			{	// xon was last sent
				
				txLinesPaused = false;	// XON
				if(stateLineByLine === 4)
					stateLineByLine = 5;	// ready to RESEND previous line due to NAK followed by XON
				else if(ackNakMode && stateLineByLine === 6)
					stateLineByLine = 3;	// ACK was rcvd and then XON now ready to send next line.
				else if(!ackNakMode){
					// legacy transfer mode
					stateLineByLine = 3;	// XON and ready to send next line.
					// update the XON indicator
					updateXOFFstatus(makeSpanColor('XON ','#191'));		
				}
				console.log('XON: ' + xon + ' ACK: ' + ack  + '  xoff: '+ xoff + ' nak: ' + nak + "  StateLBL: " + stateLineByLine);
			}else if( xoff > xon && xoff > nak && xoff > ack)
			{	// xoff was last sent
				txLinesPaused = true;		// XOFF
				if(stateLineByLine !== 4 && stateLineByLine !== 8){	// If not pending in NAK mode, nor first handshake wait
					stateLineByLine = 2;	// now waiting for ACK/XON
					// update the XOFF indicator
					updateXOFFstatus(makeSpanColor('XOFF','#911'));
				}
				console.log('XOFF: '+ xoff + '  xon: ' + xon + ' ack: ' + ack + ' nak: ' + nak  + "  StateLBL: " + stateLineByLine);
			}else
			{
				console.log('NOT xon: ' + xon + ' NOT xoff: '+ xoff + ' ack: ' + ack + ' nak: ' + nak + "  StateLBL: " + stateLineByLine);
			}
		}

        // remove any nulls
        if (rxFilterNulls) {
            data = data.replace(/\x00/g, '');
        }
        // remove any bells
        if (rxFilterBells) {
            data = data.replace(/\x07/g, '');
        }
        if (rxShowCtrls || rxShowCrtlHex || rxShowCtrlHex0 || rxShowEOLrl || rxShowEOLesc || rxShowEOLhex || rxShowTab || rxShowBackspace) {
            data = makeCTRLvisible(data);
        }
        // CR is ignored by browser,
        // LF is treated as newline by browser)
        var dataLen = data.length;
        var noBsDataStr = '';     // string with BS chars removed.
        if (rxBackspaceEnable) {
            // process any  backspace chars in data:
            var i = 0;
            var EolIdx = 0;
            var charCount = 0;
            var bsState = 0; // statemachine: 0 skipping BS chars, 2 BS seen, now collect non-BS chars, 3 need to copy nsBsDataStr to display.
            var haveData = false;
            var rxLenSave = rxLastBufferLength;
            while (i < dataLen) {// process each rx char and handle backspace like a real terminal
                var char = data.charAt(i);
                switch (bsState) {
                    case 0: // no bs seen yet state.
                        if (char === '\x08')
                        { // skip backspace and adjust insertion point.
                            if (rxInsertionIdx > rxLastEolIdx)
                                rxInsertionIdx--;
                            i++;
                        } else
                        {
                            bsState = 2;  // collect more normal chars.
                        }
                        break;
                    case 2: // seen  first data char collect more if any
                        if (char === '\x08')
                        { //  backspace so data capture done.
                            bsState = 3; // time to copy data back to bs spot.
                        } else
                        {
                            charCount++;
                            rxScrollCharCount++;
                            if (char === '\x0a' || char === '\x0d')
                            {   // Eol seen, save its location in buffer as a stopping point for BS chars.
                                EolIdx = charCount;
                                rxLastEolIdx = EolIdx + rxLastBufferLength;
                                scrollDelay = 66;
                                rxScrollEolCount++;
                                rxTimestampArmed = rxShowTimestamp;   // only arm if enabled.
                                rxScrollCharCount=0;    // reset scrol char count if getting EOLs.
                            } else if (rxShowTimestamp && rxTimestampArmed) {
                                // Timestamp to prepend
                                /*    var timestamp = new Date().toISOString();	// e.g. 2016-12-09T19:04:50.695Z
                                 timestamp=timestamp.replace("T" , " ");
                                 timestamp=timestamp.replace(/Z/ , " ");
                                 */
                                var timestamp = getTimestampString(true);
                                noBsDataStr += timestamp;
                                rxTimestampArmed = false;
                                // update start of line position for backspace handling
                                rxLastEolIdx += timestamp.length;
                            }
                            noBsDataStr += char;
                            i++;
                            haveData = true;
                        }
                        break;
                    case 3: // copy noBsDataStr to insertion point.
                        // first remove backspace amount of chars.
                        rxDataElement.textContent = rxDataElement.textContent.substring(rxCopyStartIdx, rxInsertionIdx) + noBsDataStr;
                        //rxDataElement.textContent = rxDataElement.textContent+noBsDataStr;
                        // reset for more data if any.
                        haveData = false;
                        rxInsertionIdx = rxLenSave = rxDataElement.textContent.length;
                        noBsDataStr = '';
                        bsState = 0;
                }// end of switch
            }// end of while
            if (haveData)
            {// still have data to write
                rxDataElement.textContent = rxDataElement.textContent.substring(rxCopyStartIdx, rxInsertionIdx) + noBsDataStr;
                rxInsertionIdx = rxLenSave = rxDataElement.textContent.length;
            }
            rxLastBufferLength = rxLenSave;
        } else
        {
            //rxDataElement.insertAdjacentHTML("beforeend", escapeHtml(data)); // v 0.2.3  this works, but results in CR characters getting striped out when saving file..
            rxDataElement.textContent = rxDataElement.textContent.substring(rxCopyStartIdx, rxInsertionIdx) +data;  // comment out if using above line.
            //rxDataElement.textContent += data; bug, max buffer size was not handled.
            rxInsertionIdx = rxLastBufferLength = rxDataElement.textContent.length;
            // if EOL in data schedule scroll sooner.
            (data.search(/[\x0d\x0a]/) !== -1) ? scrollDelay = 66 : scrollDelay = 330;
        }
    }
    // --------------------------
    // done with Rx processing, handle autoScroll processing
    // --------------------------
    if (logDebugLevel >= 4)// insane level
        console.log("SetRxData --START--:\n"+rxDataElement.innerHTML+"\nSetRxData ---END---\n");
    // update size display periodically.
    // Only schedule update timer if no pending update.
    if(!rxSizeTimer){
        rxSizeTimer = window.setTimeout(function () {
            document.getElementById('rxSize').textContent = rxLastBufferLength;
            document.getElementById('totalCharsRcvd').textContent = totalCharsRcvd;
            rxSizeTimer = null;
        }, 200);
    }
    // log size of RX data element at max of 2.5 sec rate.
    // Only schedule update timer if no pending update.
    if (logDebugLevel >= 1 && !rxDataLogDelay){
        rxDataLogDelay = setTimeout(function () {
            console.log('setRxData: rxdata element length:', rxLastBufferLength);
            rxDataLogDelay = null;
        }, 2500);
    }
    if (rxAutoScroll === true) {
        // new data or newline char so might need to scroll or trim buffer size.
        // bug fix: handle no EOL, and large amount of data with line wrap enabled (400 characters)
		// todo: verify the EOL and scrollCharCount is correct. Seems like it may fire rapidly...
        if (!rxScrollTimer 
				|| ( rxScrollEolCount < 24 &&  rxScrollCharCount < 400 ) 
				) 
		{	// reschedule scroll a max of 24 EOL times, or 400 chars without EOL
            clearTimeout(rxScrollTimer);  // cancel any currently running timer so we don't stack them up.
            rxScrollTimer = window.setTimeout(function () {
                // we delay 66mSec/330mSec  to give the element update time to update before scrolling to the bottom.
                rxDataElement.scrollTop = rxDataElement.scrollHeight;
                if (logDebugLevel >= 2)
                    console.log('setRxData: rxdata scrolled:  EOLcount:' + rxScrollEolCount + ' CharCount: '+ rxScrollCharCount);
                rxScrollEolCount = 0;
                rxScrollCharCount = 0;
                rxScrollTimer = null;
            }, scrollDelay);
        }
    } else
    {   // auto scroll disabled.
        clearTimeout(rxScrollTimer);
        rxScrollTimer = null;
        rxScrollEolCount = 0;
        // fix Linux bug where textarea scrolls to the top everytime data is added, restore current scroll location.
        rxDataElement.scrollTop = topScrollSave;
    }
}

// rxClearData()
// Clear the rx data  display box element and RX character count display.
function rxClearData() {
    rxDataElement.textContent = "";
    document.getElementById('rxSize').textContent = rxLastBufferLength = rxDataElement.textContent.length;
    rxInsertionIdx = rxLastBufferLength;
    console.log('rxClearData: rxdata element length:', rxLastBufferLength);
}
// ***  Handler for Chrome Serial API *****
// Callback: The handler for Rx  error events.
function onRxError(errorInfo)
{
    /* errorInfo object has two parameters:
     * errorInfo.connectionId
     * errorInfo.error
     * enum of "disconnected", "timeout", "device_lost", "break", "frame_error", "overrun", "buffer_overflow", "parity_error", or "system_error"
     */
    var statusString = "";
    var statusNL = '\n';
    if (unpauseCount === 0)
        statusNL = ' '; // clear status on first error.

    console.error('onRxError: Our ID: ' + connectionId, errorInfo);
    if (errorInfo.connectionId && errorInfo.connectionId !== connectionId)
        return;  // not our port.
    var timestamp = '';
    if (rxShowTimestamp) {
        timestamp = getTimestampString(true) + ' ';
    }
    if (document.getElementById('rxEnableShowReceiveError').checked) {
        var eString = "<" + errorInfo.error + ">";
        //console.log(eString);
        setRxData(eString);
    }
	switch (errorInfo.error) {
		case "parity_error":
		case "timeout":
			statusString = statusNL + timestamp + 'RX Error: [' + errorInfo.error + ']';
			break;
		case "break":               // seen on winblows when idle FTDI USB device unplugged. Huh?  also if baud rate wrong.
		case "buffer_overflow":     // seen on winblows when idle FTDI USB device unplugged. What?
		case "frame_error":         // seen on winblows when idle FTDI USB device unplugged. On Windows only, FTDI devices throw this error even when it is not true.
		case "overrun":
			connectionError = true;   // disable hwStatus reading.
			if( document.getElementById('rxEnableOnReceiveError').checked )
			{
				stopTxRepeat();
				statusString = statusNL + timestamp + 'RX Error: [' + errorInfo.error + '] - Check Bit Rate setting.  Or USB device disconnected';
			}else
			{
				statusString = statusNL + timestamp + 'Ignoring: RX Error: [' + errorInfo.error + ']';
				console.error('onRxError: Ignored.');
			}
			clearTimeout(hwStatusTimer);
			break;
		case "system_error":        // seen on winblows when FTDI USB device report false framing errors, this follows soon after.
		case "disconnected":        //
		case "device_lost":         // Linux reports this one on USB removal.  Which makes sense!
			connectionError = true;   // disable hwStatus reading.
			clearTimeout(hwStatusTimer);
			// close the port so we can try and reopen it.
			if (errorInfo.connectionId !== -1) {
				if (errorInfo.error === "device_lost" || unpauseCount >= rxErrorUnpauseMax || rxErrorLastError === errorInfo.error) {
					// if unpaused failed to work, close the  port that has the error
					clearTimeout(rxUnpauseTimer);

					console.log('onRxError: disconnecting ID: ' + errorInfo.connectionId);
					chrome.serial.disconnect(connectionId, function (result) {
						console.log(' onRxError: disconnect result: ' + result);
						connectionId = -1;
						unpauseCount = 0;
						rxErrorLastError = '';
						if( !autoPortReopen )
							portDropdownSelectSet('None');	// change selection if aut reopen not enabled.
						// don't change the open port name. This allows adjusting the baud rate after if fails for annoying FTDI devices on windblows.
						//openPortUniqueName = "None (Close port)";
						//portOptions.name = 'None (Close port)';
						updateHWstatus(' ');
					});
					setStatus('\n' + timestamp + 'RX Error: [' + errorInfo.error + '] - Port Closed. ('+openPortUniqueName+')', 'red');
					stopTxRepeat();
					// If autoPortReopen, start a timer to try and reopen port.
					if( autoPortReopen ) {
						console.log(" onRxError: autoPortReopen is enabled.  Will try in 1 second.");
						clearTimeout(autoPortReopenTimer);  // cancel any currently running timer so we don't stack them up.
						autoPortReopenTimer = window.setTimeout(reOpenFunc, 2000);		
					}
					return;
				}
			}
			break;
	}
    if (statusString !== "" && unpauseCount < 4) {
        setStatus(statusString, 'red');
    }
    unPausePort(connectionId);
    rxErrorLastError = errorInfo.error;
}

// keep trying to open serial port until the port opens.
var reOpenFunc = function () {
	console.log('autoPortReopen.reOpenFunc(): Try Port ReOpen');
	openSelectedPort();
	clearTimeout(autoPortReopenTimer);
	if( autoPortReopen ) {
		// keep trying until port opens. When port opens this timer is killed. Or when Port: "None" selected.
		autoPortReopenTimer=window.setTimeout(reOpenFunc, 2220);
	}
};

// unPausePort
// attempts to unPause the connection if paused.
function unPausePort()
{
    // with FTDI devices on Windows: unpause connection with FTDI devices reports successful, but very next event is a system_error.
    // However, on Linux, unpause works just fine.
    if (connectionId !== -1) {
        chrome.serial.getInfo(connectionId, function (connInfo) {
            if (chrome.runtime.lastError) {
                console.error('unPausePort: getInfo error: chrome API lastError:', chrome.runtime.lastError.message);
            }
            console.log('unPausePort: connInfo: ', connInfo);
            if (connInfo && connInfo.paused) {
                clearTimeout(rxUnpauseTimer);
                rxUnpauseTimer = setTimeout(function () {
                    chrome.serial.setPaused(connectionId, false, function () {
                        if (chrome.runtime.lastError) {
                            console.error('unPausePort: unPausePort error: chrome API lastError:', chrome.runtime.lastError.message);
                        }
                        connectionError = false;	// re-enable hwStatus reading
                        startHWsignalMonitor();		// start but delay it as much as possible.
                        unpauseCount += 1;
                        console.log('unPausePort: unpaused the ID: ' + connectionId + ' Count: ' + unpauseCount);
                    });
                    rxUnpauseTimer = null;
                }, 500);
            }
        });
    }
}
// ***  Handler for Chrome Serial API *****
// callback: called when RX data is received.
// On Windows and FTDI devices, we seem to normally get 8 byte chunks during continuous data streams.
function onRxData(RxInfo)
{
    if (RxInfo.connectionId !== connectionId) {
        return;
    }
    var value = '';
    value = ab2str(RxInfo.data);
    if (value === '')
        return;		// nothing new, ignore it.
    totalCharsRcvd += value.length;
    if (logDebugLevel >= 3)
        console.log('Serial: Rcvd: ' + value.length + ' bytes');
    lineBuffer = value;
    readBuffer += lineBuffer;
    if (logRxData) {  // log rx data if enabled.
        readLogBuffer += lineBuffer;
        clearTimeout(rxLogTimer);
        rxLogTimer = setTimeout(function () {
            // we delay before logging
            if (logRxData) {
                readLogBuffer = makeCTRLvisible(readLogBuffer);
                console.log('Serial: Rcvd: -->>>[' + readLogBuffer + ']<<<--');
            }
            readLogBuffer = '';
            rxLogTimer = null;
        }, 1000);
    }
    setRxData(readBuffer);	// update the RX data display area.and do further processing (backspace, etc).
    readBuffer = "";
}
portOptionsToString = function () {
    var shortName = /[^\s]*/.exec(portOptions.name)[0];// grab just the port path, not the description info.
//    return portOptions.name
    shortName += ' Settings: ' + portOptions.bitrate;
    if(portOptions.dataBits === 'eight')
        shortName += ',8';
    else
        shortName += ',7';
    if(portOptions.parityBit === 'no')
        shortName += ',N';
    else if(portOptions.parityBit == 'even')
        shortName += ',E';
    else if(portOptions.parityBit == 'odd')
        shortName += ',O';
    if(portOptions.stopBits ==='one')
        shortName += ',1';
    else
        shortName += ',2';
    return shortName;
};

function setPortInfo(){
    // set the portSettingsInfo element to the current port settings.
    var info = ' '
        + portOptions.bitrate;
        if(portOptions.dataBits === 'eight')
            info += ',8';
        else
            info += ',7';
        if(portOptions.parityBit === 'no')
            info += ',N';
        else if(portOptions.parityBit == 'even')
            info += ',E';
        else if(portOptions.parityBit == 'odd')
            info += ',O';
        if(portOptions.stopBits ==='one')
            info += ',1';
        else
            info += ',2';

    document.getElementById('portSettingsInfo').textContent = info;
}
// ***  OnOpen Handler for Chrome Serial API  from openSelectedPort() *****
function onOpen(openInfo) {
    if (chrome.runtime.lastError !== undefined)
    {
        console.error('onOpen(): Error attempting to open: ', chrome.runtime.lastError.message);
        setStatus('Error: Could not open port!  ' + chrome.runtime.lastError.message, 'red');
        connectionId = -1;
        openPortUniqueName = "None (Close port)";
        portOptions.name = 'None (Close port)';
        return;
    }
    connectionId = openInfo.connectionId;
    if (connectionId === -1) {
        console.error('onOpen() Error: Could not open port. connectionId: -1');
        setStatus('Error: Could not open port', 'red');
        return;
    }
    console.log('onOpen(): Port Open: ' + openInfo.name + ' UniqueName: ' + openPortUniqueName + ' connectionId: ' + connectionId);
    setStatus('Opened: ' + portOptionsToString(), 'green');
    setPortInfo();
    connectionError = false;
    unpauseCount = 0;
    rxErrorLastError = '';
    // v 0.2.3  report hand shake lines
    getHWsignals();
	// set focus to txInput box on port open
	setTxData('');
	// kill auto open timer in case that is what trigger this open.
	clearTimeout(autoPortReopenTimer);
}

/**  BEGIN  HW signal handling section **/

// getHWvl()
// get HW lines voltage level representation (RS232 voltage levels, not TTL, TTL would be inverted.)
// level - logic level of signal (true or false)
function getHWvl(level)
{
    var voltage = level ? 'V+' : 'V-';
    return voltage;
}
var hwStatusTimer = null;
var hwStatusFailCount = 0;
// get state of HW lines (CTS, DSR, etc)
// also starts a periodic timer to update HW line status in GUI.
function getHWsignals(noLog)
{
    var cON = '#191';// on/off colors
    var cOFF = '#911';
    var string = " ";
    if (connectionId !== -1 && !connectionError && rxEnableShowHWLines) {
        try {
            chrome.serial.getControlSignals(connectionId, function (signals) {
                if (chrome.runtime.lastError) {// will get here on Linux with socat virtual tty.
                    if (hwStatusFailCount < 4) {
                        hwStatusFailCount++;
                        console.error('getHWsignals(' + noLog + '): chrome API lastError:', chrome.runtime.lastError.message);
                        setStatus('\nChrome API: ' + chrome.runtime.lastError.message, 'red');
                    }
                    string += makeSpanColor("&nbsp; [Failed to get HW control signals]", cOFF);

                } else {
                    hwStatusFailCount = 0;
                    if (!noLog)
                        console.log('getHWsignals(' + noLog + '): ID:' + connectionId + ' Port Lines: ', signals);
                    if (signals) {
                        string += makeSpanColor(" CTS:&nbsp;" + signals.cts + "(" + getHWvl(signals.cts) + ")", signals.cts ? cON : cOFF);
                        string += "&nbsp;&nbsp;";
                        string += makeSpanColor(" DSR:&nbsp;" + signals.dsr + "(" + getHWvl(signals.dsr) + ")", signals.dsr ? cON : cOFF);
                        string += "&nbsp;&nbsp;";
                        string += makeSpanColor(" DCD:&nbsp;" + signals.dcd + "(" + getHWvl(signals.dcd) + ")", signals.dcd ? cON : cOFF);
                        string += "&nbsp;&nbsp;";
                        string += makeSpanColor(" RI:&nbsp;" + signals.ri + "(" + getHWvl(signals.ri) + ")", signals.ri ? cON : cOFF);
                    }
                    // schedule another update
                    startHWsignalMonitor();
                }
                updateHWstatus(string);
            });
        } catch (err) {
            console.log('getHWsignals(): catch error:', err);
        }
    } else {
        updateHWstatus(' ');//no connection or disabled so blank display.
    }
}
// used to re-start hw monitoring if stopped while port still open. 2.6 sec polling cycle.
function startHWsignalMonitor() {
    clearTimeout(hwStatusTimer);
    hwStatusTimer = setTimeout(function () {
        getHWsignals(true); // don't log the periodic monitoring.
    }, 2600);
}
function makeSpanColor(string, color)
{
    var spanStr = '<span style="color:' + color + '">' + string + '</span>';
    return spanStr;
}

/**  END  HW signal handling section **/

// build_portDropdown(ports)
// ports - an array of strings with a path to a port.
// build a drop down list of the /dev/tty ports, supports both winblows and linux/MAC
// delete any existing element first to handle the refresh case.
// Filter any port that has productId & vendorId IDs of 0. Newer version of nw.js return non-valid ports in list.
function build_portDropdown(ports) {
    console.log('build_portDropdown: raw list: ', ports);
    var eligiblePorts = ports.filter(function (port) {
        //    return !port.path.match(/[Bb]luetooth/) && (port.path.match(/\/dev\/tty/) || port.path.match(/COM[\d+]/));
        return  (port.path.match(/\/dev\/tty/) || port.path.match(/COM[\d+]/));
    });
    var portCount = 0;
    var portDropdown = document.getElementById('port-dropdown');
    removeOptions(portDropdown);    // delete any previous list.
    var portOption = document.createElement('option');
    console.log('build_portDropdown() dropdown:');
    portOption.value = portOption.textContent = 'None';
    portOption.textContent += ' (Close port)';
    portOption.className = 'dropdown-none';
    portDropdown.appendChild(portOption);
    console.log(' added: None');
    eligiblePorts.forEach(function (port) {
		// second filter: skip invalid ports.
		if(port.hasOwnProperty('productId') && port.productId !== 0)
		{
			portOption = document.createElement('option');
			portOption.value = portOption.textContent = port.path;
			if (port.displayName) {
				portOption.textContent += ' [' + port.displayName + ']';
			}
			portDropdown.appendChild(portOption);
			portOption.className = 'dropdown-standard';
			console.log(' added: ' + portOption.textContent);
			portCount++;
		}
    });
    if (customPorts)
        customPorts.forEach(function (port) {
            if (port && port.path) {
                portOption = document.createElement('option');
                portOption.value = portOption.textContent = port.path;
                if (port.displayName) {
                    portOption.textContent += ' [' + port.displayName + ']';
                }
                portOption.className = 'dropdown-custom';
                portDropdown.appendChild(portOption);
                console.log(' custom added: ' + portOption.textContent);
                portCount++;
            }

        });
    // add a "refresh" item at the end:
    portOption = document.createElement('option');
    portOption.value = portOption.textContent = 'Refresh';
    portOption.textContent += ' port list';
    portOption.className = 'dropdown-refresh';
    portDropdown.appendChild(portOption);
    console.log(' added: Refresh');
    // setup the onchange handle for the dropdown we built.
    portDropdown.onchange = function () {
        console.log('portDropdown.onchange: ');
        openSelectedPort();
    };
    return portCount;
}

// select the item in the dropdown based on passed in value.
// This only sets the dropDown box value, does not open a port!
function portDropdownSelectSet(portValue)
{
    document.getElementById('port-dropdown').value = portValue;
    /* How we would do it if item name is passed in
     var dd = document.getElementById('port-dropdown');
     for (var i = 0; i < dd.options.length; i++) {
     if (dd.options[i].text === portName) {
     dd.selectedIndex = i;
     break;
     }
     }*/
    console.log('portDropdownSelectSet: ' + portValue);
}
function refreshPortDropdown()
{   // this will close any open port.
    chrome.serial.getDevices(function (ports) {
        if (chrome.runtime.lastError) {
            console.error('refreshPortDropdown(): getDevices error: chrome API lastError:', chrome.runtime.lastError.message);
        }
        var pc = build_portDropdown(ports);
        openSelectedPort();
        setStatus("Port List Refreshed. Ports found: " + pc, 'blue');
    });
}
// open the port that is the selected port in the GUI dropdown
function openSelectedPort() {
    // first close any open port
    if (connectionId !== -1) {
        // close any port that was open and clear info displays
        console.log('openSelectedPort(): disconnecting ID: ' + connectionId);
        clearTimeout(rxUnpauseTimer);
        stopTxRepeat();
        chrome.serial.disconnect(connectionId, function (result) {
        if (chrome.runtime.lastError) {
            console.error('openSelectedPort(): disconnect error: chrome API lastError:', chrome.runtime.lastError.message);
        }
            console.log('openSelectedPort(): disconnect ID: ' + connectionId + '  result: ' + result);
        });
        setTxData("");
        connectionId = -1;
        updateHWstatus(' ');
        //return;
    }
    var portDropdown = document.getElementById('port-dropdown');
    var selectedPort = portDropdown.options[portDropdown.selectedIndex].value;
    openPortUniqueName = portDropdown.options[portDropdown.selectedIndex].label;
    console.log('openSelectedPort(): Port: ' + selectedPort + ' UniqueName: ' + openPortUniqueName);
    var color = portDropdown.options[portDropdown.selectedIndex].className;
    portDropdown.className = color;
    portDropdown.blur();

    if (selectedPort.match(/Refresh/)) {
        refreshPortDropdown();
    } else
    {
        portOptions.name = openPortUniqueName;
        if (portSettingsArray)
        {
            var portSettingObject = portSettingsArray.find(d => d.portUniqueName === openPortUniqueName);
            if (portSettingObject) {
                // TODO: remove next line after a few versions:  This is to fix a bug in data stored.
                portSettingObject.portSettings.name = openPortUniqueName;
                portOptions = Object.assign({}, portSettingObject.portSettings);// make a clone of the object

                console.log('  found previous port settings for: ' + openPortUniqueName, portOptions);
            } else
            {
                // New port we havent seen yet.
                console.log('  NO previous port settings for: ' + openPortUniqueName, portOptions);
                portSettingsArray.push({portUniqueName: openPortUniqueName, portSettings: Object.assign({}, portOptions)});
                chrome.storage.local.set({portSettingsArray: portSettingsArray}, function () {
                    console.log('  openSelectedPort(): First time opening this port, saved port settigns.', portOptions);
                    ;
                });
            }
        }
        if (!selectedPort.match(/None/)) {
            setStatus('Waiting for Port: ' + selectedPort, 'orange');
            //        portOptions.name = selectedPort;
            chrome.serial.connect(selectedPort, portOptions, onOpen);
        } else
        {   // port closed.
            connectionId = -1;
            portOptions.name = 'None (Close port)';
            openPortUniqueName = "None (Close port)";
            stopTxRepeat();
            setStatus('Port Closed.', 'red');
            updateHWstatus(' ');
			// kill auto open timer in case that was running.
			clearTimeout(autoPortReopenTimer);
        }
    }
}
// remove ALL options from a drop down list.
// using the function:
// removeOptions(document.getElementById("mySelectObject"));
function removeOptions(selectbox)
{
    var i;
    i = selectbox.options.length;
    console.log('removeOptions(): removing: ' + i, selectbox);
    for (i = i - 1; i >= 0; i--) {
        selectbox.remove(i);
    }
}

function rxEnableShowHWLinesOnChange(element)
{
    rxEnableShowHWLines = element.checked;
    console.log('rxEnableShowHWLinesOnChange: ', rxEnableShowHWLines, element);
    chrome.storage.local.set({rxEnableShowHWLines: rxEnableShowHWLines}, function () {
        ;
    });
    getHWsignals();
}

function rxShowTimestampOnChange(element)
{
    rxShowTimestamp = element.checked;
    rxTimestampArmed = rxShowTimestamp;   // only arm if enabled.
    console.log('rxShowTimestampOnChange: ', rxShowTimestamp, element);
    chrome.storage.local.set({rxShowTimestamp: rxShowTimestamp}, function () {
        ;
    });
}

function rxAllHexDumpOnChange(element)
{
    rxAllHexDump = element.checked;
    console.log('rxAllHexDumpOnChange: ', rxAllHexDump, element);
    chrome.storage.local.set({rxAllHexDump: rxAllHexDump}, function () {
        ;
    });
}
var tabLabel = document.getElementById('rxShowTabLbl');
var backspaceLabel = document.getElementById('rxShowBackspaceLbl');
function rxShowCtrlsOnChange(element)
{
  var myI = " ";

  console.log('rxShowCtrlsOnChange: element ID: ', element.id);
  console.log('  tabLabel: ',tabLabel);
  console.log('  backspaceLabel: ',backspaceLabel);
  {
    console.log('rxShowCtrlsOnChange: element checked: ', element.checked);
    switch (element.id) {
      case 'rxShowCtrls':   // show as <mnemonic> e.g <Bel>  <ESC>
        myI += "rxShowCtrls ";
        rxShowCtrls = element.checked;
        if( element.checked === true ){
        // reset others
            rxShowCrtlHex = false;
            rxShowCtrlHex0 = false;
            // update tab check text to <HT>
            tabLabel.innerText = "Tab as <HT>";
            // update backspace check text
            backspaceLabel.innerText = "Bckspc as <BS>";
        }
        break;
      case 'rxShowCrtlHex': // \xXX
        myI += "rxShowCrtlHex ";
        rxShowCrtlHex = element.checked;
        if( element.checked === true ){
        // reset others
            rxShowCtrls = false;
            rxShowCtrlHex0 = false;
            // update tab check text to \x09
            tabLabel.innerHTML = "Tab as \\x09";
            // update backspace check text
            backspaceLabel.innerHTML = "Bckspc as \\x08";
        }
        break;
      case 'rxShowCtrlHex0': // \0xXX
        myI += "rxShowCtrlHex0 ";
        rxShowCtrlHex0 = element.checked;
        if( element.checked === true ){
        // reset others
            rxShowCtrls = false;
            rxShowCrtlHex = false;
            // update tab check text to \x09
            tabLabel.innerHTML = "Tab as \\0x09";
            // update backspace check text
            backspaceLabel.innerHTML = "Bckspc as \\0x08";
        }
        break;
      case 'rxShowEOLrl':
        myI += "rxShowEOLrl ";
        rxShowEOLrl = element.checked;
        if( element.checked === true ){
        // reset others
            rxShowEOLesc = false;
            rxShowEOLhex = false;
            // update tab check text to \t
            tabLabel.innerHTML = "Tab as \\t";
            // update backspace check text
            backspaceLabel.innerHTML = "Bckspc as \\b";
        }else
            updateTabBsCheckText();
        break;
      case 'rxShowEOLesc':  // <CR> <LF>
        myI += "rxShowEOLesc ";
        rxShowEOLesc = element.checked;
        if( element.checked === true ){
        // reset others
            rxShowEOLrl = false;
            rxShowEOLhex = false;
            // update tab check text to <HT>
            tabLabel.innerText = "Tab as <HT>";
            // update backspace check text
            backspaceLabel.innerText = "Bckspc as <BS>";
        }else
            updateTabBsCheckText();
        break;
      case 'rxShowEOLhex':  // EOL hex will match ctrl hex format.
        myI += "rxShowEOLhex ";
        rxShowEOLhex = element.checked;
        if( element.checked === true ){
        // reset others
            rxShowEOLrl = false;
            rxShowEOLesc = false;
            updateTabBsCheckText();
        }
        break;
      case 'rxShowTab': // tab format will match EOL format (<Tab>, /t), or Ctrl format
        myI += "rxShowTab ";
        rxShowTab = element.checked;
        break;
      case 'rxShowBackspace': // backspace format will match EOL format (<BS>, /b), or Ctrl format
        myI += "rxShowBackspace ";
        rxShowBackspace = element.checked;
        break;

      default:
        myI += " Default! ";
        break;
    }
    // update local storage
    var checklist = {rxShowCtrls: rxShowCtrls,rxShowCrtlHex: rxShowCrtlHex,
                    rxShowCtrlHex0: rxShowCtrlHex0,rxShowEOLrl: rxShowEOLrl,
                    rxShowEOLesc: rxShowEOLesc,rxShowEOLhex: rxShowEOLhex,
                    rxShowTab: rxShowTab,
                    rxShowBackspace: rxShowBackspace
                };
    chrome.storage.local.set(checklist , function () {
        console.log(' rxShowCtrlsOnChange:  saved checkList: ', checklist);
        updateCtrlCheckElements(checklist);
    });
  }
  console.log('rxShowCtrlsOnChange: Case ' + myI);
}

function updateCtrlCheckElements(checklist){
    // iterate through the check list to update page elements.
    if(checklist){
        console.log('updateCtrlCheckElements() list: ',checklist);
        for (var key in checklist) {
            //console.log(key, checklist[key]);
            document.getElementById(key).checked = checklist[key];
        }
    }else{
        console.error(' Error: no checklist.');
    }
}

function updateTabBsCheckText(){
    // update tab text.
        if(rxShowEOLrl){
            // update tab check text to \t
            tabLabel.innerHTML = "Tab as \\t";
            backspaceLabel.innerHTML = "Bckspc as \\b";
        }else if(rxShowEOLesc || rxShowCtrls){
            // update tab check text to <HT>
            tabLabel.innerText = "Tab as <HT>";
            backspaceLabel.innerText = "Bckspc as <BS>";
        }else if(rxShowCrtlHex)
        {
            // update tab check text to \x09
            tabLabel.innerHTML = "Tab as \\x09";
            backspaceLabel.innerHTML = "Bckspc as \\x08";
        }else if(rxShowCtrlHex0)
        {
            // update tab check text to \x09
            tabLabel.innerHTML = "Tab as \\0x09";
            backspaceLabel.innerHTML = "Bckspc as \\0x08";
        }
}
function rxFilterNullsOnChange(element)
{
    rxFilterNulls = element.checked;
    console.log('rxFilterNullsOnChange: ', rxFilterNulls, element);
    chrome.storage.local.set({rxFilterNulls: rxFilterNulls}, function () {
        ;
    });
}

function rxFilterBellsOnChange(element)
{
    rxFilterBells = element.checked;
    console.log('rxFilterBellsOnChange: ', rxFilterBells, element);
    chrome.storage.local.set({rxFilterBells: rxFilterBells}, function () {
        ;
    });
}

// -------------------------------
// tx functions below....
// -------------------------------
function setTxData(data) {
    txInputElement.value = data;
    // force cursor to be at the end of the just inserted string: needs to be delayed to work.
    setTimeout(function () {
        var caretPos = 999999;
        txInputElement.focus();
        txInputElement.setSelectionRange(caretPos, caretPos);
    }, 20);
}
function getTxData() {
    return txInputElement.value;
}
function appendTxData(data) {
    var txSendStr;
    txSendStr = getTxData();
    txSendStr += data;
    setTxData(txSendStr);
}
//--------------------------------------------------------------------
// startTxRepeat()
// delay for a while...then resend last command
// called when the repeat checkbox is ticked.
//
var lastSentStr = '';
var txRepeatTimer = null;
function startTxRepeat()
{
    var baseCycle = 1000; // mSecs
    var cycleCount = txRepeatDelay;
    var repeatTimes = txRepeatCount;
    if (cycleCount < 1)
    {// adjust baseCycle rate to 100mSec ticks
        baseCycle = 100;
        cycleCount *= 10;
    }
    clearInterval(txRepeatTimer);
    // setup timer to count down at 1 Sec intervals
    txRepeatTimer = setInterval(function () {
        var prefixStat =""; // used to prefix status with tab if needed.
        if (lastSentStr !== "") {
            if (logDebugLevel > 1 && (cycleCount < 3 || cycleCount === txRepeatDelay))     // only log last 2 seconds, or reload of start value
                console.log('startTxRepeat(): ' + cycleCount);
            --cycleCount;
            if (cycleCount <= 0) {
                sendSerial(lastSentStr, true);		// fire off string again.
                //prefixStat = "\t";
                cycleCount = txRepeatDelay;
                if (cycleCount < 1)
                {
                    cycleCount *= 10; // adjust for 100mSec baseCycle rate.
                }
                if (txRepeatCount !== 0) {
                    if (--repeatTimes <= 0)
                        stopTxRepeat(); // completed the number of TX repeat cycles.
                } else
                    repeatTimes++;  // forever mode, so use as count of how many tx times.
            }
            if (parseFloat(txRepeatDelay) > 0.2)
                setStatus(prefixStat+' Auto repeat delay: ' + (cycleCount) + "  Count: " + repeatTimes, 'blue');
        } else
        {
            stopTxRepeat(); // there was nothing to send
        }
    }, baseCycle);
    // log starting count.
    console.log('startTxRepeat(): timerID: ' + txRepeatTimer + '  rate: ' + txRepeatDelay + "  Count: " + txRepeatCount);
    return txRepeatTimer;
}

function stopTxRepeat()
{
    if (txRepeatTimer) {
        console.log('stopTxRepeat(): TimerID: ', txRepeatTimer);
        clearInterval(txRepeatTimer);
    }
    setRepeatMode(false);   // v 1.6.2.8  to prevent it from restarting if Options menu is entered again.
}

var txQikClicker=true;
function init_txButtons()
{
    // must get called only once on page load with clicker === true.  Otherwise multiple click handlers get attached.
    var btnNmb = 1;
    var theButton;
    var eName = 'qscmd' + btnNmb;
    // hook up click event for ALL buttons, and initialize all button values & names to defaults
    while((theButton = document.getElementById(eName))){
        // using theButton.onClick = function(e){quickSendBtn(this);};  Did not work in Chrome App.
        if(txQikClicker)
            theButton.addEventListener('click', function(){quickSendBtn(this);});
        theButton.value = 'Button ' + btnNmb;
        theButton.name = btnNmb;
        console.log('build_txButtons('+btnNmb+'):  ',theButton);
        btnNmb++;
        eName = 'qscmd' + btnNmb;
    }
    txQikClicker = false;
}

var btnNmb = 1;
function build_txButtons()
{
    // setup defaults for the buttons
    init_txButtons();
    // add customization to the buttons as needed.
    btnNmb = 1;
    var eName = 'qscmd' + btnNmb;
    if (customTxButtons)
        customTxButtons.forEach(function (btn) {
            console.log('build_txButtons():  button: ' + btnNmb, btn);
            eName = 'qscmd' + btnNmb;
            theButton = document.getElementById(eName);
            if(theButton){
                if (btn && btn.displayName) {
                    theButton.value = btn.displayName;
                } else {
                    theButton.value = 'Button ' + btnNmb;
                }

            }
            btnNmb++;
        });
    txQuickVisibility();
}

function txQuickVisibility()
{
    if(txQuickHide){
        // hide the row of quick send buttons also eliminates space taken by content of div.
        document.getElementById('quickBtnsGrp1Div').style.display  = 'none';
        document.getElementById('quickBtnsGrp2Div').style.display  = 'none';
        document.getElementById('quickBtnsGrp3Div').style.display  = 'none';
    }else{
            // show the next row of quick send buttons
        document.getElementById('quickBtnsGrp1Div').style.display  = 'block';
        if(btnNmb >6){
            // show the next row of quick send buttons
            document.getElementById('quickBtnsGrp2Div').style.display = 'block';
            if(btnNmb >11){
                // show the next row of quick send buttons
                document.getElementById('quickBtnsGrp3Div').style.display = 'block';
            }else{
                // hide the next row of quick send buttons also eliminates space taken by content of div.
                document.getElementById('quickBtnsGrp3Div').style.display  = 'none';
            }
        }else{
            // hide the next row of quick send buttons also eliminates space taken by content of div.
            document.getElementById('quickBtnsGrp2Div').style.display  = 'none';
            document.getElementById('quickBtnsGrp3Div').style.display  = 'none';
        }
    }
    resizeThrottler();  // need to resize after adding or removing a row of buttons.
}

function txQuickHideOnChange(element){
    txQuickHide = element.checked;
    console.log('txQuickHideOnChange: ', txQuickHide, element);
    chrome.storage.local.set({txQuickHide: txQuickHide}, function () {
        ;
    });
    txQuickVisibility();
}
/* ******************************************** */
/* *******  Start of TxHistory functions ****** */
/* ******************************************** */
var txHistorySaving = true;    // set to true to enable saving tx history to local storage. Changable in options.html
var txHistory = new Array(64);  // 64 undefined elements in ring buffer.
var txHistoryHead = -1;
var txHistoryTail = 0;
var txHistorySelect = -1;       // no history selected
var txHistoryLengthMax = txHistory.length;
var txHistoryLength = 0;        // number of valid enteries.
console.log('txHistory.length: ' + txHistoryLengthMax);

// Let's grab any stored history from local storage
restoreTxHistory();

// txHistoryUpdate  call it with a string to put into the tx history ring buffer.
function txHistoryUpdate(txStr)
{
    if (!txStr || txStr.length < 2 || txStr.length > 255)
        return;// empty, null, NaN, 0, false, or undefine string. Or string too short or too long
    if (txHistoryHead === -1) {// first time.
        txHistoryLength = 0;
        txHistoryHead = 0;
        txHistory[txHistoryHead++] = ''; // need blank line so tx input box can be blanked if don't want history.
        txHistoryLength++;
        txHistory[txHistoryHead] = txStr;
        txHistoryLength++;
    }
    // check if history was browsed, might be a historical repeat
    if (txHistorySelect !== txHistoryHead) {
        if (txStr === txHistory[txHistorySelect])
            return; // retransmit of history item currently selected.
        txHistorySelect = -1; // something new, reset history selector.
    }
    if (txStr !== txHistory[txHistoryHead]) { // not same as previous
        txHistoryHead++;
        txHistoryHead = txHistoryHead % txHistoryLengthMax;
        if (txHistoryHead === txHistoryTail) {
            txHistoryTail++; // wrapped around so move tail.
            txHistoryTail = txHistoryTail % txHistoryLengthMax;
            //  insert blank at tail.
            txHistory[txHistoryTail] = ''; // need blank line so tx input box can be blanked if don't want history.
        }
        txHistory[txHistoryHead] = txStr;
        if (txHistoryLengthMax > txHistoryLength)
            txHistoryLength++;
        txHistorySelect = -1;
    }else
    {
        if (logDebugLevel >= 2)
            console.log('txHistoryUpdate: Duplicate! select: ' + txHistorySelect + ' H:' + txHistoryHead + ' T:' + txHistoryTail + ' L:' + txHistoryLength + '  ', txHistory);
            return;
    }

    if (logDebugLevel >= 2)
        console.log('txHistoryUpdate: select: ' + txHistorySelect + ' H:' + txHistoryHead + ' T:' + txHistoryTail + ' L:' + txHistoryLength + '  ', txHistory);
    // save updated history data to local storage.
    saveTxHistory();
} // end of txHistoryUpdate()

// makes and returns an object with the txHistory data needed for saving.
// used for both local storage saving and export to file feature in options.js.
function makeTxHistorySaveObject()
{
    var txHistoryData = {} ;
    console.log('makeTxHistorySaveObject(); entered.');
    // todo: should we clone the objects instead?
    // txHistoryData['history'] = Object.assign({}, txHistory);// make a clone of the object
    txHistoryData['history'] = txHistory;
    txHistoryData['length'] = txHistoryLength;
    txHistoryData['head'] = txHistoryHead;
    txHistoryData['tail'] = txHistoryTail;
    txHistoryData['maxLength'] = txHistory.length;
    return txHistoryData;
}
// saveTxHistory()
// Save the txHistory data to local storage.
// Uses the txHistory global variables.
//
function saveTxHistory(){
    // ToDo: may want to schedule this on a timer to delay the actual save until system idle??
    if(txHistorySaving===true){
        var txHistoryData = makeTxHistorySaveObject();
        chrome.storage.local.set({'txHistoryData': txHistoryData },function() {
            if (chrome.runtime.lastError) {
                console.error('Error: saveTxHistory(): chrome API lastError:', chrome.runtime.lastError.message);
                return;
            }
            console.log('saveTxHistory(): completed.');
        });
    }else
        console.log('saveTxHistory(): saving disabled.');
}
// initTxHistory()
// called by both restore from local storage and restore from import.
function initTxHistory(txHistoryData){
    console.log('initTxHistory(): data in: ',txHistoryData);
    var i =0;
    if(typeof txHistoryData !== "undefined"){
        // copy data to our global vars
        // copy array item by item in case array size stored is different that software size.
        var txArray = txHistoryData['history']  ;
        var saveLength = txHistory.length;
        var saveLengthMax = txHistoryData['maxLength'];
        txHistory.length = 0;               // wipe clean the txHistory array before initializing it.
        txHistory.length = saveLength;      // set it back to original length, and it will all be empty.  Its like javascript magic.
        while(i < txArray.length && i < txHistory.length && txArray[i]!=null){
            txHistory[i] = txArray[i++];
        }
        txHistoryLength = txHistoryData['length']  ;
        txHistoryHead = txHistoryData['head']  ;
        txHistoryTail = txHistoryData['tail']  ;
        // if array size was increased from what was last stored, we need to adjust.
        // note: bug: if head and tail wrapped around, then some items may get lost.
        if(txHistoryLengthMax > saveLengthMax && txHistoryData['tail'] !== 0){
            console.log('initTxHistory(): app array larger than stored. Making tail&head adjustments: new size: '+txHistoryLengthMax);
            txHistory[txHistoryTail] = txHistory[0]; // move zeroth item to current tail.
            txHistoryTail  = 0;             // point tail at zeroth item
            txHistory[txHistoryTail] = "";  // make new tail location blank.
            txHistoryHead = txHistoryLength-1;
        }else if (txHistoryLengthMax < txHistoryLength){
             console.log('initTxHistory(): app array smaller than stored. Making tail&head adjustments: new size: '+txHistoryLengthMax);
            // need to shrink pointers.
            txHistoryTail  = 0;             // move tail to zeroth item
            txHistory[txHistoryTail] = "";  // make new tail item.
            txHistoryHead = txHistoryLengthMax-1;
            txHistoryLength = txHistoryLengthMax;
        }
        // don't need to recover max length, since we need to use what the app has.
        //txHistoryLengthMax = txHistoryData['maxLength']  ;

        // reset selection to initial condition
        txHistorySelect = -1;
    }else
        console.log('initTxHistory(): No saved history data.');
    console.log('initTxHistory() done: Items initialized: '+i+'  txHLength: '+txHistoryLength+'\ntxHHead: '+txHistoryHead+'    txHTail: '+txHistoryTail+'    txHLMax: '+txHistoryLengthMax+'\ntxHistory: \n',txHistory);
}

// restoreTxHistory()
// restore from local storage, call this when the app is first opened.
function restoreTxHistory(){
    console.log(MyFileName + ': restoreTxHistory(): Starting.  Saving state: '+txHistorySaving+'  current txHistory: \n', txHistory);
    if( txHistorySaving===true )
        chrome.storage.local.get('txHistoryData',function(se){
            if (chrome.runtime.lastError) {
                console.error('Error: restoreTxHistory(): chrome API lastError:', chrome.runtime.lastError.message);
                return;
            }
            console.log('restoreTxHistory(): recalled from local storage: ', se);
            var txHistoryData = se.txHistoryData;
            initTxHistory(txHistoryData);
            console.log('restoreTxHistory(): completed.');
        });
    else{
        console.log('restoreTxHistory(): saving disabled.');
        removeTxHistory();  // make sure to remove any.
    }
}

// removeTxHistory()
// remove any saved tx history data from the local storage.
function removeTxHistory(){
    chrome.storage.local.remove('txHistoryData',function(){
        if (chrome.runtime.lastError) {
            console.error('Error: removeTxHistory(): chrome API lastError:', chrome.runtime.lastError.message);
            return;
        }
        console.log('removeTxHistory() completed.');
    });
}

// fetch a string from the tx history buffer, by first moving in the direction request.
// direction is 'up' or 'down'
function txHistoryGet(direction)
{
    if (logDebugLevel >= 1)
        console.log("txHistoryGet: Dir: " + direction + " select:" + txHistorySelect + " H:" + txHistoryHead + " T:" + txHistoryTail + " L:" + txHistoryLength);
    if (txHistoryLength === 0)
        return ''; // no history .
    //
    // up starts at head.  down starts tail
    if (txHistorySelect === -1 && direction === 'up') {
        txHistorySelect = txHistoryHead; // init for first recall at head
    } else if (txHistorySelect === -1 && direction === 'down') {
        txHistorySelect = -1; // like linux - retun blank.  was: //txHistorySelect = txHistoryTail; // init for first recall at tail
    } else if (direction === 'up' && (txHistorySelect !== txHistoryTail || txHistorySelect === txHistoryHead)) {
        txHistorySelect--;
        if (txHistorySelect < 0){
            // need to handle case where deletes where done on some items and head has wrapped around
            if( txHistoryHead < txHistoryTail){
                // head wrapped around so wrap at buffer max length.
                txHistorySelect = txHistoryLengthMax - 1;
            }else{
                // Head not wrapped around, so wrap select around used part of buffer length
                txHistorySelect = txHistoryLength - 1;
            }

        }
    } else if (direction === 'down') {//&& (txHistorySelect !== txHistoryTail)){// || txHistorySelect === txHistoryTail)){
        if(txHistorySelect === txHistoryHead){
            // reset to initial condition so we return blank.
            txHistorySelect = -1;
        }else{
            txHistorySelect++;
            // need to handle case where deletes where done on some items and head has wrapped around
            if( txHistoryHead < txHistoryTail){
                // head wrapped around so wrap at buffer max length.
                txHistorySelect %= txHistoryLengthMax;
            }else{
                // Head not wrapped around, so wrap select around used part of buffer length
                txHistorySelect %= txHistoryLength;
            }
        }
    }
    if (direction === 'down' && (txHistorySelect === txHistoryTail)) {
        // reset to initial condition so we return blank.
        txHistorySelect = -1;
    }
//    txHistorySelect %= txHistoryLength;
    if (logDebugLevel >= 1)
        console.log("txHistoryGet: select: " + txHistorySelect, txHistory);
    if (txHistorySelect === -1)
        return "";
    return txHistory[txHistorySelect];
} // end of txHistoryGet

// -----------------------------------------------------------------------------
// txHistoryDeleteCurrent()
// delete the currently selected history item
// and adjust various pointers to match the new layout of the circular buffer
function txHistoryDeleteCurrent()
{
	if (logDebugLevel >= 1)
        console.log("txHistoryDeleteCurrent(): select:" + txHistorySelect + " H:" + txHistoryHead + " T:" + txHistoryTail + " L:" + txHistoryLength + " arrL: " + txHistory.length);

	if (txHistorySelect > -1 && txHistorySelect != txHistoryTail) {
            var savedHead = txHistoryHead;

            txHistory.splice(txHistorySelect, 1); // remove one item from the array
            txHistoryLength--;
            if( txHistoryHead > txHistoryTail){
                // if head has not wrapped around
                // insert new null element at the head to keep actual array size constant.
                txHistory.splice(txHistoryHead, 0, null);
                // adjust head pointer back one.
                txHistoryHead--;
                if(txHistoryHead < 0)
                        txHistoryHead = txHistoryLength > 0 ? txHistoryLength-1 : 0;
            }else{
                if(txHistorySelect <= txHistoryHead){
                    var savedHead = txHistoryHead;
                    // special case handle deleting at or below the head, need to point to new head
                    txHistoryHead--;
                    if(txHistoryHead < 0)
                        txHistoryHead = txHistoryLengthMax-1;

                }
                // head has wrapped around, need to insert new element AFTER head.
                // insert new "" element after head to keep actual array size constant. Note: Can't insert null otherwise that will mess up saving code.
                txHistory.splice(txHistoryHead+1, 0, "");
                if(txHistoryTail < txHistorySelect){
                // adjust tail pointer such that it still points at ""
                    txHistoryTail++;
                    txHistoryTail %= txHistoryLengthMax;
                }
            }
            if(txHistorySelect === savedHead){
                // adjust select if needed
                txHistorySelect = txHistoryHead;
            }
            // save the history to local storage
            saveTxHistory();
            setTxData('');	// blank out the recalled text that was just deleted from history
            console.log("txHistoryDeleteCurrent(): deleted:" + txHistorySelect + " H:" + txHistoryHead + " T:" + txHistoryTail + " L:" + txHistoryLength + " arrL: " + txHistory.length, txHistory);
	}
}
/* *************************************** */
/* *****  End of TxHistory functions ***** */
/* *************************************** */

var txRTS = true;     // a checkbox.
var txDTR = true;     // a checkbox
var txAddCR = true;   // a check box
var txAddLF = true;   // a check box
var txAutoClear = false; // a check box. Clear teh Tx Input box when data is sent.
var txImmediate = false; // a check box. Send characters as they are typed, don't wait for Enter key or Send button.
var txImmediatePaused = false; // if history recalled in txImmediate mode, then pause that mode.
var txEcho = false;   // local echo flag.
// when checkbox is toggled change the RTS signal level
function txRTSOnChange(element)
{
    txRTS = element.checked;
    console.log('txRTSOnChange: ', txRTS, element);
    updateHWstatus();
    var voltage = txRTS ? 'V+' : 'V-';
    if (connectionId !== -1)
        setStatus('RTS line set to: ' + txRTS + ' (' + voltage + ')');
}

// when checkbox is toggled change the DTR signal level
function txDTROnChange(element)
{
    txDTR = element.checked;
    console.log('txDTROnChange: ', txDTR, element);
    updateHWstatus();
    var voltage = txDTR ? 'V+' : 'V-';
    if (connectionId !== -1)
        setStatus('DTR line set to: ' + txDTR + ' (' + voltage + ')');
}

// when checkbox is toggled change the CR setting
function txAddCROnChange(element)
{
    txAddCR = element.checked;
    console.log('txAddCROnChange: ', txAddCR, element);
    chrome.storage.local.set({txAddCR: txAddCR}, function () {
        ;
    });
}
// when checkbox is toggled change the LF setting
function txAddLFOnChange(element)
{
    txAddLF = element.checked;
    console.log('txAddLFOnChange: ', txAddLF, element);
    chrome.storage.local.set({txAddLF: txAddLF}, function () {
        ;
    });
}
// when checkbox is toggled change the auto clear setting
function txAutoClearOnChange(element)
{
    txAutoClear = element.checked;
    console.log('txAutoClearOnChange: ', txAutoClear, element);
    chrome.storage.local.set({txAutoClear: txAutoClear}, function () {
        ;
    });
    setTxData('');
}
// when checkbox is toggled change the TX Immediate setting
function txImmediateOnChange(element)
{
    txImmediate = element.checked;
    console.log('txImmediateOnChange: ', txImmediate, element);
    chrome.storage.local.set({txImmediate: txImmediate}, function () {
        ;
    });
    setTxData('');
    txImmediatePaused = false;
    if (txImmediate) {
        txAutoClear = true;
        document.getElementById('txAutoClear').checked = txAutoClear;
    }
}
// when checkbox is toggled change the auto Echo setting
function txEchoOnChange(element)
{
    txEcho = element.checked;
    console.log('txEchoOnChange: ', txEcho, element);
    chrome.storage.local.set({txEcho: txEcho}, function () {
        ;
    });
}


// handle keyboard input if focus is on Rx data window,
// if not a ctrl char put the keypresses into the txInput box.
// and call txKeypress for normal and ctrl chars
function rxKeypress(evt) {
    var txchar = '';
    if (logDebugLevel >= 3)
        console.log('Rx rxKeypress evt: ', evt);
    if (evt.which !== 13 && !evt.ctrlKey) {
        // normal key...but not control chars like: ^D, ^J, ^S ^Q (not ^A, ^C, ^v, ^W)
        if (evt.key !== undefined) {
            txchar = evt.key;
        } else if (evt.charCode !== undefined) {// for chrome on linux, no .key element
            txchar = String.fromCharCode(evt.charCode);
        } else {
            setStatus('Runtime error! rxKeypress() ', 'red');
            console.error('Runtime error! rxKeypress() evt: ', evt);
        }
        appendTxData(txchar);
    }
    txKeypress(evt); // finish up like it was typed in tx input box.
}
// handle key down when focus is rx window.
// Use to get escape key, backspace, Fn keys
// which is not available in rxKeypress event.
function rxKeydown(evt)
{
	var txchar = null;
    if (logDebugLevel >= 3)
        console.log('rxKeydown evt: ', evt);
	if( !evt.altKey && !evt.shiftKey && !evt.ctrlKey){
        // handle up/down arrow keys with no meta key
        switch (evt.which) {
			case 27:    // escape \x1b
                txchar = '\x1b';	// send esc even if not in tx immediate mode.
                break;
            case 112:   // F1 - F10 map to the first 10 quick buttons
            case 113:
            case 114:
            case 115:
            case 116:
            case 117:
            case 118:
            case 119:
            case 120:
            case 121: // F10
                sendQuickBtnData(evt.which-111);
                return;
		}
	}
	if (txchar) {
		sendSerial(txchar);
	}
}
// handles a key down when focus is in the tx input box.
// this event does NOT get arrow keys, escape key, backspace, Fn keys
var txDataSave;
function txKeypress(evt)
{
    var txData = '';
    if (txImmediate) {
        if (logDebugLevel >= 3)
            console.log('TxImmediate txKeypress evt: ', evt);
        if (evt.which === 13 && !evt.altKey && !evt.shiftKey && !evt.ctrlKey) {
            // enter key hit. Now check if history just recalled:
            if (logDebugLevel >= 3)
                console.log('TxImmediate txKeypress. Enter key. txHistorySelect: '+txHistorySelect);
            if (txHistorySelect !== -1)
            {
                txData = getTxData();
                txData = replaceEOLs(txData);
                txHistorySelect = -1;
            } else {
                //  Enter key: add string to history first
                txHistoryUpdate(getTxData());
            }
            // delay before blanking textarea box, the CR gets in there later
            setTimeout(function () {
                setTxData('');
            }, 30);
            // send end of line char(s) if needed for this CR event.
            if (txAddCR || !txAddLF)
                txData += '\x0d';
            if (txAddLF)
                txData += '\x0a';
        } else {// ctrl-Enter (LF) also handled here
            if (logDebugLevel >= 3){
                console.log('TxImmediate txKeypress. Handle non-Enter key ');
			}
            // normal key...or some control chars like: ^D, ^J, ^S ^Q (not ^A, ^C, ^v, ^W)
            // must hold Shift+Ctrl for ^A, ^C, ^D,...  ^W,... (not ^v)
            if (evt.charCode !== undefined) {// get most ctrl chars this way.
                txData = String.fromCharCode(evt.charCode);
            } else {
                setStatus('Runtime error! txKeypress() ', 'red');
                console.error('Runtime error! txKeypress() evt: ', evt);
            }
        }
        sendSerial(txData);
        getHWsignals((logDebugLevel > 0 ? false : true));   // update hand shake line status
        return;
		// end of if (txImmediate)
    } else if (evt.which === 13) {  // enter key hit. send string from tx input box
        txDataSave = getTxData();     // save copy without the ending LF that the enter key inserts in the textarea.
        if (logDebugLevel >= 1)  console.log('txKeypress: called with Enter. txAutoClear: '+txAutoClear);
        sendTxData();               // send data in the tx input box.
        // if not auto-clear, restore input box without the LF that the enter key slipped in there.
        if (!txAutoClear)
            setTimeout(function () {// must delay before restoring, otherwise weirdness happens
                setTxData(txDataSave);
            }, 30);
    } else {
        if (logDebugLevel >= 3){
            console.log('txKeypress: called with something: ', evt);
		}
		// normal key...or some control chars like: ^D, ^J, ^S ^Q (not ^A, ^C, ^v, ^W)
		// must hold Shift+Ctrl for ^A, ^C, ^D,...  ^W,... (not ^v)
		if (evt.charCode !== undefined ) {// get most ctrl chars this way.
			if( evt.ctrlKey)
				// always send ctrl char immediately
				sendSerial(String.fromCharCode(evt.charCode));
		} else {
			setStatus('Runtime error! txKeypress() ', 'red');
			console.error('Runtime error! txKeypress() evt: ', evt);
		}
    }
}
// keydown event handler, this is how we can get arrow keys. And escape key, backspace, Fn keys
// Up-arrow is which=38   Down-arrow is which=40
function txKeydown(evt)
{
    if (logDebugLevel >= 3)
        console.log('txKeydown evt: ', evt);
    var dir = 'up';
    var str=null;
	var txchar = null;
    if( !evt.altKey && !evt.shiftKey && !evt.ctrlKey){
        // handle up/down arrow keys with no meta key
        switch (evt.which) {
			case 27:    // escape \x1b
                txchar = '\x1b';	// send esc even if not in tx immediate mode.
                break;
            case 40:    // down-arrow
                dir = 'down';
            case 38:    // up-arrow
                str = txHistoryGet(dir);
                setTxData(str);
                if (logDebugLevel >= 3)
                    console.log('txKeydown up/down arrow. txImmediate: '+txImmediate+' txHistorySelect:'+txHistorySelect+' txImmediatePaused:'+txImmediatePaused);
                // need to pause txImmediate mode if history just recalled a non-empty string. This allows editing the string before TX
                if (txImmediate && txHistorySelect !== -1) {
                    txImmediatePaused = true;
                    txImmediate = false;
                }else if(txImmediatePaused && (txHistorySelect === -1 || str === '')) {
                    // unpause if recalled blank line
                    txImmediatePaused = false;
                    txImmediate = true;
                }
                if (logDebugLevel >= 3)
                    console.log('txKeydown up/down arrow. txImmediate: '+txImmediate+' txHistorySelect:'+txHistorySelect+' txImmediatePaused:'+txImmediatePaused);
                return;
            case 112:   // F1 - F10 map to the first 10 quick buttons
            case 113:
            case 114:
            case 115:
            case 116:
            case 117:
            case 118:
            case 119:
            case 120:
            case 121: // F10
                sendQuickBtnData(evt.which-111);
                return;
        }
    }else if(!evt.altKey && !evt.shiftKey && evt.ctrlKey){ // handle Ctrl-keys
		switch(evt.which) {
			case 46:
			// Ctrl-DEL will remove the current txHistory item from the history buffer.
				txHistoryDeleteCurrent();
				break;
		}
	}
    if (txImmediate) {// handle special keys, such as; arrow keys, escape which:27, backspace which:8  and delete which:46,  Tab which:9 keys

        switch (evt.which) {
            case 27:    // escape \x1b
                txchar = '\x1b';
                break;
            case 8:     // backspace
                txchar = '\x08';
                break;
            case 9:     // tab key
                txchar = '\x09';
                // tab move focus to next html input element, so
                // move focus back to tx input box and add the tab char to the tx input box.
                appendTxData(txchar);
                break;
            case 13: // handle ALT-Enter (CR)
                if( evt.altKey && !evt.shiftKey && !evt.ctrlKey){
                    txchar = '\x0d'; // cr
                }
                break;
            case 46:    // delete key (without Alt, Shift or Ctrl keys)
				if( !evt.altKey && !evt.shiftKey && !evt.ctrlKey){
							txchar = '\x7f';
				}
                break;
            case 38:    // shift--up-arrow - ANSI arrow key sequence
                if( !evt.altKey && evt.shiftKey && !evt.ctrlKey) str = "\x1b[A";
                break;
            case 40:    // shift--down-arrow
                if( !evt.altKey && evt.shiftKey && !evt.ctrlKey) str = "\x1b[B";
                break;
            case 39:    // shift--right-arrow
                if( !evt.altKey && evt.shiftKey && !evt.ctrlKey) str = "\x1b[C";
                break;
            case 37:    // shift--left-arrow
                if( !evt.altKey && evt.shiftKey && !evt.ctrlKey) str = "\x1b[D";
                break;
        }
    }
	if (txchar) {
		sendSerial(txchar);
		getHWsignals((logDebugLevel > 0 ? false : true));   // update handshake line status
	}else if(str){// send any ANSI arrow key sequences
		sendSerial(str);
	}
}
function txPaste(evt)
{
    if (txImmediate) {// after paste event need to delay before we can get pasted content to send
        setTimeout(function () {
            var txData = null;
            console.log('Tx txPaste evt: ', evt);
            txData = getTxData();
            txData = replaceEOLs(txData);
            if (txData) {
                sendSerial(txData);
                getHWsignals();   // update handshake line status
            }
        }, 30);
    }
}

// replaceHexBinEscapes( Str )
// Replace escape sequence \xHH or\XHH or \bXXXXXXXX, \n, \r, \t with byte value
//   also accepts \0xHH or \0bXXXXXXXX  by adding 0{0,1}? to the regex.
// Replace  \\ with \
// Do NOT replace \\xHH or \\bXXXXXXXX
function replaceHexBinEscapes(Str) {
    Str = Str.replace(/\\\\|[\\]{1}0{0,1}?[xX][0-9A-Fa-f]{2}|[\\]{1}0{0,1}?b[0-1]{8}|[\\]{1}n|[\\]{1}r|[\\]{1}t/g, makeHexBin);
    // the \\\\ is there to help ignore the corner case where \\ is used
    // to escape a backslash and it is followed by one of the key characters (xbnrt) or by escape sequence \xHH , etc..
    // Here is a bit of handy regex kit: https://regex101.com/  our regex: https://regex101.com/r/p6zQN4/1
    return Str;
}
// called by String.replace() so function argument parameters must be as they are.
// See here for more details: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/replace
function makeHexBin(match, offset, string) {
    var byte = 0;
    var mIdx = 1; // index to conversion type char (x, X, b, n , r, t, ...)
    var nIdx = 2; // index to start of digits for x, X, or b types.
    if (logDebugLevel >= 2)
        console.log('makeHexBin: offset:' + offset, ' match: [' + match + ']', ' input: [' + string + ']');
    if (match[1] === '0') { // adjust offsets to skip leading zero case.
        mIdx++;
        nIdx++;
    }
    if (match[mIdx] === 'x' | match[mIdx] === 'X') {
        byte = parseInt(match.substring(nIdx), 16);// parse as base 16
    } else if (match[mIdx] === 'b') {
        byte = parseInt(match.substring(nIdx), 2); // need 8 of 0 or 1 (base 2)
    } else if (match[mIdx] === 'n') {
        byte = 0x0a;    // newline - linefeed.
    } else if (match[1] === 'r') {
        byte = 0x0d;    // carriage return.
    } else if (match[1] === 't') {
        byte = 0x09;    // tab
    } else if (match[1] === '\\') {
        return match.substring(1); // Replace  \\ with \ along with remaining char (xXbnr or t) after the \
    }
    return String.fromCharCode(byte);
}
// Replace CR that keyboard puts in input box to that value
// currently set by the GUI checkboxes.
function replaceEOLs(str) {
    // replace EOL char(s) based on flags.
    if (!txAddCR && txAddLF) {
        str = str.replace(/\x0d\x0a/g, "\x0a");
        str = str.replace(/\x0a\x0d/g, "\x0a");
        str = str.replace(/\x0d/g, "\x0a");
    } else if (txAddCR && !txAddLF) {
        str = str.replace(/\x0d\x0a/g, "\x0d");
        str = str.replace(/\x0a\x0d/g, "\x0d");
        str = str.replace(/\x0a/g, "\x0d");
    } else if (txAddCR && txAddLF) {
        str = str.replace(/\x0a/g, "\x0d\x0a");// will only get LF from pasting into html box.
        str = str.replace(/\x0d\x0d/g, "\x0d");// clean up CR CR LF in case input already had CR LF
    }
    return str;
}

function sendQuickBtnData(btnNmbr)
{
    if (logDebugLevel >= 3) console.log('sendQuickBtnData(): #',btnNmbr);
    if (customTxButtons[btnNmbr - 1] && customTxButtons[btnNmbr - 1].displayName)
        sendCmd(customTxButtons[btnNmbr - 1].data);
    else
        setStatus('Button '+btnNmbr+' not configured. (Go to Options to configure)', 'red');
    getHWsignals();   // update hand shake line status
}

// a quick send button clicked...
function quickSendBtn( element)
{
    var btnNmbr = +element.name;
    console.log('quickSendBtn(): #'+btnNmbr+' clicked!', element);
    sendQuickBtnData(btnNmbr);
}
// Send Button handler
// Send the data in the Tx input box
function sendTxData()
{
    var str = getTxData();
    console.log('sendTxData: called.');
    sendCmd(str);
    if (txAutoClear) {
        // delay before blanking textarea box, the LF gets in there later
        setTimeout(function () {
            setTxData('');
        }, 30);
    }
    if(txImmediatePaused){ // restore mode
        if (logDebugLevel >= 3)
            console.log('sendTxData: restore txImmediateMode to true. ');
            txImmediatePaused = false;
            txImmediate = true;
            txHistorySelect = -1;
    }
    getHWsignals();   // update hand shake line status
}

// Send a command string and record in history buffer
// This func adds/replaces EOLs to/in the input string
//  changes escaped hex or binary to actual byte values in the input string
//  and then sends it
function sendCmd(inStr)
{
    txHistoryUpdate(inStr); // record string as is.  Recalled strings go through here again.
	sendCmdNoHistory(inStr)
}

// Send a command string, do NOT record in history buffer
// This func adds/replaces EOLs to/in the input string
//  changes escaped hex or binary to actual byte values in the input string
//  and then sends it
//  nostatus - true/false whether to update the status line or not.
function sendCmdNoHistory(inStr, nostatus)
{
    var txData = "";
    txData = replaceEOLs(inStr); // replace any EOL in the string.
    txData = replaceHexBinEscapes(txData); // handle any hex or binary escape sequences
    // add any requested EOL chars at end of line
    if (txAddCR)
        txData += '\x0d';
    if (txAddLF)
        txData += '\x0a';
    sendSerial(txData, nostatus);
}

// send a string as is (raw), no EOLs added/modified, no Hex/Bin escaping...
// nostatus:
//    if nostatus false;  update the status element with   Bytes Sent:  <bytes>
var txStatusJiggle = 0;
var txBusy = false;
function sendSerial(txData, nostatus)
{
    if (connectionId < 0) {
        setStatus('Send Error: No serial port open!', 'red');
        console.error('sendSerial: No serial port open!');
        return;//throw 'Invalid connection';
    }
    if (txData) {
        totalCharsXmtd += txData.length;
        txStatusJiggle ^= 1;
        var statStr = (txStatusJiggle ? '' : '  ') + 'Bytes Sent: ' + txData.length;
        if (!nostatus)
            setStatus(statStr, 'orange');
        txBusy = true;// todo: use to determine if need to queue data.
        chrome.serial.send(connectionId, str2ab(txData), function () {
            if (chrome.runtime.lastError) {
                console.error('sendSerial: send error: chrome API lastError:', chrome.runtime.lastError.message);
            }

            txBusy = false;
            document.getElementById('totalCharsXmtd').textContent = totalCharsXmtd;
            if (!txRepeatMode || logDebugLevel >= 2)
                console.log('sendSerial:Callback: ' + statStr);
			if( txLinesToSend > 0 )	// Xfr active, update Bytes per second
			{
				var timeNow = new Date();
				var timeElapsed = timeNow - txFileTimeStart;	// milliseconds.
				var bytesPerSecond = (totalCharsXmtd/timeElapsed)*1000;
				var bps = Math.floor(bytesPerSecond);
				document.getElementById('txFileBPS').textContent = bps.toString();
				console.log('Bytes: ' + totalCharsXmtd + ' TimeElapsed: ' + timeElapsed + ' BPS: ' + bps.toString());
			}
        });
        if ((!txRepeatMode && logDebugLevel >= 2) || logDebugLevel >= 3)
            console.log('sendSerial: Queued to Send: >>[' + makeCTRLvisible(txData) + ']<<');
        lastSentStr = txData;
        if (txEcho) {
            // echo tx string to rxData area
            setRxData(txData);
        }
    } else
        console.error('sendSerial: no txData! ', txData);
}

/** BEGIN app frame resizing methods **/
var lastReSizeWidth = 0;
var lastReSizeHeight = 0;
var resizeTimeout;
function  resizeThrottler() {
    // ignore resize events as long as an actual ResizeHandler execution is in the queue
    if (!resizeTimeout) {
        resizeTimeout = setTimeout(function () {
            resizeTimeout = null;
            console.log('resizeThrottler fired! Window Width: ' + window.innerWidth);
            reSizeTxInput();
            reSizeRxData();
            // The actualResizeHandler will execute at a rate of 10fps if 100 used.
        }, 50);
    }
}
function  reSizeTxInput() {
    var width = window.innerWidth;
    if (width > 235 && width !== lastReSizeWidth) {
        // resize if widow is wide enough
        lastReSizeWidth = width;
        width = (width - 200); // leave room for the tx send text and button.
        console.log('reSizeTxInput: W:' + width);
        txInputElement.setAttribute("style", "width:" + width + 'px');
    }
}
function reSizeRxData() {
    var containerHeight = document.getElementById('container').offsetHeight;
    var rxdataHeight = rxDataElement.offsetHeight;
    var reserved = containerHeight - rxdataHeight + 20;
    var rxheight;
    var height = window.innerHeight; //height=getAppHeight();
    rxheight = height - reserved;// subtract off room for the objects above.
    // new for textarea instead of div: need to adjust width as well.
    var width = window.innerWidth - 35;
    if (width < 130)
        width = 130;
    if (rxheight > 23 && rxheight !== lastReSizeHeight) {
        lastReSizeHeight = rxheight;
        //    rxDataElement.setAttribute("style", "width:"+ width +'px;'+" height:"+ rxheight +'px');
        console.log('reSizeRxData: AH:' + height + ' CH:' + containerHeight + ' R:' + reserved + ' rxH:' + rxheight);
    }
    rxDataElement.setAttribute("style", "width:" + width + 'px;' + " height:" + lastReSizeHeight + 'px;');
    rxAreaAppyWrapMode();   // need to restore wrap style settings
    fontSizeOnChange(null); // need to restore font size style setting
    guiThemeOnChange(null); // need to restore color theme style
    tabWidthOnChange(null); // need to restore tab width style
}
/** END  app frame resizing methods **/
function setFontSizeDropdown( size ) {
    document.getElementById('fontSize').value = size;
}
function fontSizeOnChange( element ){
	var fontSize = document.getElementById('fontSize').value;
    console.log("fontSizeOnChange: font size: " + fontSize);
    rxDataElement.style.fontSize = fontSize;
    txInputElement.style.fontSize = fontSize;
	chrome.storage.local.set({rxtxFontSize: fontSize}, function () { ; });
}
function setTabWidthDropdown( width ) {
    document.getElementById('tabWidth').value = width;
}
function tabWidthOnChange( element ) {
    var tabWidth = document.getElementById('tabWidth').value;
    console.log("tabWidthOnChange: tab width: " + tabWidth);
    rxDataElement.style.tabSize = tabWidth;
    chrome.storage.local.set({rxTabWidth:tabWidth}, function() {;});
}
function setGuiThemeDropdown( theme ) {
    document.getElementById('guiTheme').value = theme;
}
function guiThemeOnChange( element ){
	var guiTheme = document.getElementById('guiTheme').value;
    console.log("guiThemeOnChange: gui theme: " + guiTheme);
	switch( guiTheme ){
		default:
		case "ig":	// its green on black
			rxDataElement.style.color = '#4f3';
			rxDataElement.style.backgroundColor = 'black';
			break;
		case "pn":	// purple night
			rxDataElement.style.color = '#F633FF';
			rxDataElement.style.backgroundColor = '#000';
			break;
		case "bn":	// blue night
			rxDataElement.style.color = '#33B5FF';
			rxDataElement.style.backgroundColor = 'black';
			break;
		case "on":	// orange night
			rxDataElement.style.color = '#f63';
			rxDataElement.style.backgroundColor = 'black';
			break;
		case "bb":	// bubble bee
			rxDataElement.style.color = 'yellow';
			rxDataElement.style.backgroundColor = 'black';
			break;
		case "bs":	// bloodshot
			rxDataElement.style.color = 'red';
			rxDataElement.style.backgroundColor = 'white';
			break;
		case "bw":	// boring black & white
			rxDataElement.style.color = 'black';
			rxDataElement.style.backgroundColor = 'white';
			break;
		case "bp":	// blue print
			rxDataElement.style.color = '#2312e8';
			rxDataElement.style.backgroundColor = 'white';

			break;
	}
	chrome.storage.local.set({rxGuiTheme: guiTheme}, function () { ; });
}

/*
 * Send a blob of text line by line to the serial port.
 * Assumes serial port is open (this does not check).
 * A Send can be aborted by setting the variable txLinesToSend to 0.
 *
 * @param {type} textBlob
 * @returns {undefined}
 */
function sendTextByLine(textBlob)
{
	// send a blob of text line by line with a pause between lines.
	// make an array of lines to send.
	var textByLine = textBlob.split("\n");
	txLinesToSend = textByLine.length;
	txLinesTotal = txLinesToSend; // save a copy for later logging.
	txFileTimeStart = new Date();
	totalCharsXmtd = 0;
	lineToSend = 0;	// start with the first line in the array.
	stateLineByLine = 8;	// state is: waiting for first handshake to determine if using xon/xoff or ack/nak mode.
	ackNakMode = false;		// default to legacy mode.
	pausedLogTime = new Date();
	console.log("sendTextByLine() Total Lines: " + txLinesToSend + "Line delay: " + txLineDelay +" mSec");
	var reentrant = function() {
		if(textByLine[lineToSend].length > 0 && (txLinesPaused || stateLineByLine === 1 ))
		{
			// in waiting for xoff state or in xoff state.
			var timeNow = new Date();
			var timeDiff = timeNow - pausedLogTime; //in ms
			if(timeDiff > 700) {
				console.log("sendTextByLine() timeDiff: " + timeDiff +  "  PAUSED on Line#: "+(lineToSend+1)+ "  StateLBL: " + stateLineByLine );
				if(logDebugLevel >= 1)
					console.log(" paused:text: ["  + textByLine[lineToSend] +"]");
				pausedLogTime = new Date();
			}
			// output is paused, so try again later
			setTimeout(reentrant, txLineDelay);
            return;
		}
		if(stateLineByLine === 3 && !txLinesPaused)
		{
			stateLineByLine = 0;	// waited for and got ACK/XON (without NAK), now ready to send.
			lineToSend++;			// Next line can be sent.
			pausedLogTime = new Date();
		}else if(stateLineByLine === 5 && !txLinesPaused)
		{
			// if NAK followed by XON then re-send previous line
			console.log("sendTextByLine() Re-send Line# "+ (lineToSend+1) + " due to NAK ********");
			stateLineByLine = 0;	// Ready to send
			if(logDebugLevel >= 1)
				console.log(" re-send: ["  + textByLine[lineToSend] +"]");
		}
		// If not aborted, and ready to send, send a line ....
		if(stateLineByLine === 0  && textByLine[lineToSend].length > 0)
		{
			// in case on windblows, remove any CR at end of line (and LF too).
			textByLine[lineToSend] = textByLine[lineToSend].replace(/\s*$/, "");
			console.log("sendTextByLine() Line#: "+ (lineToSend+1) );
			if(logDebugLevel >= 2)
				console.log("text: ["  + textByLine[lineToSend] +"]");
			sendCmdNoHistory(textByLine[lineToSend], true); // send the line but no status line update.
			document.getElementById('txFileSentLines').textContent = (lineToSend+1);
			stateLineByLine = 1;	// wait for XOFF state
		}
		
        if (lineToSend < txLinesToSend) {
			// more lines to send, schedule the next line...
            setTimeout(reentrant, txLineDelay);
            return;
        }
		// get here if transfer completed, or if it was aborted.
		if( stateLineByLine === 9 ) {
			console.log("sendTextByLine() ABORTED. Lines: "+(lineToSend) +" SENT  of " + txLinesTotal + " Total");
			setStatus("Send File: ABORTED", 'orange');
		}else{
			console.log("sendTextByLine() Finished. Lines: "+(lineToSend) +" SENT  of " + txLinesTotal + " Total");
			setStatus("Send File: COMPLETED", 'green');
		}
		// hide abort button and show send button
		document.getElementById('txAbortSend').style.display  = 'none';
		// display completed progress for 2 seconds.
		setTimeout(function(){
			// hide progress and abort button after 2 seconds.
			document.getElementById('tXSendFile').style.visibility = 'visible';
			document.getElementById('txSendFileProgress').style.display  = 'none';
			txLinesToSend = 0;
			txLinesPaused = false;
			stateLineByLine = 0;
		},2011);
    };
	document.getElementById('txFileSendTotal').textContent = txLinesToSend;
	// hide the Send File button and show the Abort Send button
	// note: we use two different methods to hide and show buttons here.
	// Need to use "style.visibility' on SendFile button to prevent it from shifting location.
	// Need to use 'style.display' on AbortSend button to allow it to be visible. (It starts life as hidden)
	document.getElementById('tXSendFile').style.visibility = 'hidden';
	document.getElementById('txAbortSend').style.display  = 'block';
	document.getElementById('txSendFileProgress').style.display  = 'block';

	updateXOFFstatus(makeSpanColor('XON ','#191'));
	// trigger resize handler to adjust window layout after button hide/unhide above.
	resizeThrottler();
    setTimeout(reentrant, 10);	// Schedule the send of the first line in 10 mSec
}

function updateXOFFstatus(string)
{
	var statusElement = document.getElementById('txFileSendXOFF');
        statusElement.innerHTML = string;
}

// =============================================================
// on page load:
//  setup event handlers, load saved settings and enumerate serial ports.
onload = function () {
    document.getElementById('appName').textContent = manifest.name;
    document.getElementById('verId').textContent = "V" + manifest.version; //
    console.log("onload() Starting ...");
    // load settings

    updateFromStorage();
    rxClearData();
    setTxData('');
    // ==================================================================================
    // hook up send button
    document.querySelector('#txSend').addEventListener('click', sendTxData);
    // flush button
    document.querySelector('#txrxFlush').addEventListener('click', function(){chrome.serial.flush( connectionId, function( result) {console.log('flush(): ',result);});});
	// Abort sending file button
   document.querySelector('#txAbortSend').addEventListener('click', function(){console.log('AbortSendingButton(): '+ txLinesToSend ); txLinesToSend=0; txLinesPaused=false; stateLineByLine = 9; updateXOFFstatus(makeSpanColor('XOFF','#911'));});
     // hook up an options button.
    document.querySelector('#go-to-options').addEventListener('click', function () {
        // Reasonable fallback.
        console.log("options.html will be opened.");
        chrome.app.window.create('options.html', {
            innerBounds: {//outerBounds: {
                top: 70,
                left: 70,
                width: 680,
                height: 408 // 448 // 484
            }
        });
    });
    // hook up rx related checkboxes
    // display modes drop down
    document.getElementById('rxAutoScroll').onchange = function () {
        rxAutoScrollOnChange(this);
    };
    document.getElementById('rxAutoWrap').onchange = function () {
        rxAutoWrapOnChange(this);
    };
    document.getElementById('rxShowTimestamp').onchange = function () {
        rxShowTimestampOnChange(this);
    };
    document.getElementById('rxFilterNulls').onchange = function () {
        rxFilterNullsOnChange(this);
    };
    document.getElementById('rxFilterBells').onchange = function () {
        rxFilterBellsOnChange(this);
    };
    document.getElementById('rxAllHexDump').onchange = function () {
        rxAllHexDumpOnChange(this);
    };
    // unprintables drop down
    document.getElementById('rxShowCtrls').onchange = function () {
        rxShowCtrlsOnChange(this);
    };
    document.getElementById('rxShowCrtlHex').onchange = function() {
      rxShowCtrlsOnChange(this);
    };
    document.getElementById('rxShowCtrlHex0').onchange = function() {
      rxShowCtrlsOnChange(this);
    };
    document.getElementById('rxShowEOLrl').onchange = function() {
      rxShowCtrlsOnChange(this);
    };
    document.getElementById('rxShowEOLesc').onchange = function() {
      rxShowCtrlsOnChange(this);
    };
    document.getElementById('rxShowEOLhex').onchange = function() {
      rxShowCtrlsOnChange(this);
    };
    document.getElementById('rxShowTab').onchange = function() {
      rxShowCtrlsOnChange(this);
    };
    document.getElementById('rxShowBackspace').onchange = function() {
      rxShowCtrlsOnChange(this);
    };
    // other checkboxes
    document.getElementById('rxEnableOnReceiveError').onchange = function () {
        rxEnableOnReceiveErrorOnChange(this);
    };
    document.getElementById('rxEnableShowReceiveError').onchange = function () {
        rxEnableShowReceiveErrorOnChange(this);
    };
    document.getElementById('rxEnableShowHWLines').onchange = function () {
        rxEnableShowHWLinesOnChange(this);
    };

    document.getElementById('txQuickHide').onchange = function () {
        txQuickHideOnChange(this);
    };

    // tx input box and rx box auto resizing
    resizeThrottler();
    window.addEventListener('resize', resizeThrottler); // auto resize
    //
    // tx EOL and other checkboxes
    document.getElementById('txAddCR').onchange = function () {
        txAddCROnChange(this);
    };
    document.getElementById('txAddLF').onchange = function () {
        txAddLFOnChange(this);
    };
    document.getElementById('txAutoClear').onchange = function () {
        txAutoClearOnChange(this);
    };
    document.getElementById('txImmediate').onchange = function () {
        txImmediateOnChange(this);
    };
    document.getElementById('txEcho').onchange = function () {
        txEchoOnChange(this);
    };

    document.getElementById('txRTS').onchange = function () {
        txRTSOnChange(this);
    };
    document.getElementById('txDTR').onchange = function () {
        txDTROnChange(this);
    };
    document.getElementById('txRTS').checked = txRTS;
    document.getElementById('txDTR').checked = txDTR;
    // tx input actions.
    txInputElement.addEventListener('keypress', function (evt) {
        txKeypress(evt);
    });
    txInputElement.addEventListener('keydown', function (evt) {
        txKeydown(evt);
    });
    txInputElement.addEventListener("paste", function (evt) {
        txPaste(evt);
    });
    document.getElementById('rxClear').onclick = function () {
        readBuffer = "";
        rxClearData();
        totalCharsRcvd = 0;         // reset total character counts.
        totalCharsXmtd = 0;
        document.getElementById('totalCharsRcvd').textContent = totalCharsRcvd;
        document.getElementById('totalCharsXmtd').textContent = totalCharsXmtd;
    };

    // grab chars if typed in rx area
    rxDataElement.addEventListener('keypress', function (evt) {
        rxKeypress(evt);
    });
	// for getting escape key
	rxDataElement.addEventListener('keydown', function (evt) {
        rxKeydown(evt);
    });
    // open about page if icon clicked.
    document.getElementById('myIcon').addEventListener('click', function (e) {
        //increaseLogLevel(); // moved to about.js.
        chrome.app.window.create('about.html', {
            innerBounds: { //outerBounds: {
                top: 70,
                left: 70,
                width: 480,
                height: 320 //270
            }
        });
    });
    document.getElementById('fontSize').onchange = function() {
		fontSizeOnChange(this);
    };
    setFontSizeDropdown("10px"); // set drop down to match css style (default)

    document.getElementById('tabWidth').onchange = function() {
                tabWidthOnChange(this);
    };
    setTabWidthDropdown("8"); // set drop down to match css style (default)
    document.getElementById('guiTheme').onchange = function() {
		guiThemeOnChange(this);
    };
    setGuiThemeDropdown("ig"); // set drop down to match css style (default)
    //
    // ****  Chrome API setup ****
    // setup RX data event handler
    chrome.serial.onReceive.addListener(onRxData);
    // on error handler
    if (rxEnableOnReceiveError)
        chrome.serial.onReceiveError.addListener(onRxError);

    chrome.serial.getDevices(function (ports) {
        build_portDropdown(ports);
        openSelectedPort();
        setStatus("Ready!");
    });
	// try and force focus to the txInput box so typing right away after app opens will be seen delay additional 100 mSec to make sure it is last operation.
	setTimeout(function () {
                setTxData('');
            }, 100);
    console.log('logDebugLevel: ' + logDebugLevel + '  logRxData: ' + logRxData);
    console.log("onload() DONE.  " + manifest.name + "  " + manifest.version+"   ------------------------------------------ ");
};
// This function is also called from options.js on a save click.
// updatePortList - if true : refresh the port list if no open port.
//                  if undefined / false do not update list.
function updateFromStorage(updatePortList) {
    console.log(MyFileName + ": updateFromStorage(): Load persistent storage");
    // get items configured from the options page.
    chrome.storage.local.get(optionsStorageIDs, function (items) {
        console.log(MyFileName + ": updateFromStorage():  Option items: ", items);
        // set fall back port values. Can probably remove in the future
        portOptions.bitrate = +items.serialRate;
        portOptions.dataBits = items.dataBits;
        portOptions.parityBit = items.dataParity;
        portOptions.stopBits = items.dataStopBits;
        portOptions.ctsFlowControl = items.ctsFlowControl;

        // get port settings array
        portSettingsArray = items.portSettingsArray;
        if (portSettingsArray)
        {
            var portSettingObject = portSettingsArray.find(d => d.portUniqueName === openPortUniqueName);
            if (portSettingObject) {
                // TODO: remove next line after a few versions:  This is to fix a bug in data stored.
                portSettingObject.portSettings.name = openPortUniqueName;
                // cloning object fixed issue of object portOptions being a reference, but
                // Todo: might need to do deep cloning: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/assign
                portOptions = Object.assign({}, portSettingObject.portSettings);// make a clone of the object
                console.log(MyFileName + ': updateFromStorage(): found previous port settings for: ' + openPortUniqueName, portOptions);
            }
        }
        txRepeatDelay = +items.repeatDelay; // force item to be a number with the plus sign.
        txRepeatCount = +items.repeatCount;
        //repeatMode = items.repeatMode;

		txLineDelay = +items.txLineDelay;

        customPorts = items.customPorts;
        customTxButtons = items.customTxButtons;
        setMaxRxBufferLength(items.maxRxBufferSz);
        if (connectionId !== -1)
        {
            // we have an open connection, so update the parameters for it;
            chrome.serial.update(connectionId, portOptions, function (result) {
                console.log(MyFileName + ': updateFromStorage(): updated: Port: ' + portOptions.name + '  connectionId: ' + connectionId + '  result: ' + result, portOptions);
                if (result){
                    setStatus('Updated: ' + portOptionsToString(), 'blue');
                    setPortInfo();
                }else {
                    setStatus('Port Update: Failed!', 'red');
                }
                console.log(MyFileName + ': updateFromStorage(): updated: result: ' + result + ' lastError:', chrome.runtime.lastError);
            });
        } else if (updatePortList)
        {   // no open port, so it is safe to refresh port list to show new custom ports that may've been added.
            console.log(MyFileName + ': updateFromStorage(): No Open Ports, so refresh port list...');
            refreshPortDropdown();
        }
        build_txButtons();
    });
    // update the main UI page items.
    chrome.storage.local.get(mainStorageIDs, function (items) {
        console.log(MyFileName + ": updateFromStorage(): main UI items: ", items);
        // set tx checkboxes
        document.getElementById('txAddCR').checked = txAddCR = items.txAddCR;
        document.getElementById('txAddLF').checked = txAddLF = items.txAddLF;
        document.getElementById('txAutoClear').checked = txAutoClear = items.txAutoClear;
        document.getElementById('txImmediate').checked = txImmediate = items.txImmediate;
        document.getElementById('txEcho').checked = txEcho = items.txEcho;
        document.getElementById('txQuickHide').checked = txQuickHide = items.txQuickHide; // set default value
        txQuickVisibility();    // make sure to show quick buttons if enabled.
        // set rx checkboxes
        document.getElementById('rxAutoScroll').checked = rxAutoScroll = items.rxAutoScroll; // set default value
        document.getElementById('rxAutoWrap').checked = rxAutoWrap = items.rxAutoWrap; // set default value
        document.getElementById('rxFilterNulls').checked = rxFilterNulls = items.rxFilterNulls;
        document.getElementById('rxFilterBells').checked = rxFilterBells = items.rxFilterBells;

        document.getElementById('rxShowCtrls').checked = rxShowCtrls = items.rxShowCtrls;
        document.getElementById('rxShowCrtlHex').checked = rxShowCrtlHex = items.rxShowCrtlHex;
        document.getElementById('rxShowCtrlHex0').checked = rxShowCtrlHex0 = items.rxShowCtrlHex0;
        document.getElementById('rxShowEOLrl').checked = rxShowEOLrl = items.rxShowEOLrl;
        document.getElementById('rxShowEOLesc').checked = rxShowEOLesc = items.rxShowEOLesc;
        document.getElementById('rxShowEOLhex').checked = rxShowEOLhex = items.rxShowEOLhex;
        document.getElementById('rxShowTab').checked = rxShowTab = items.rxShowTab;
        document.getElementById('rxShowBackspace').checked = rxShowBackspace = items.rxShowBackspace;

        updateTabBsCheckText();
        setFontSizeDropdown( items.rxtxFontSize);
		fontSizeOnChange(null);

		setGuiThemeDropdown( items.rxGuiTheme);
        guiThemeOnChange(null);

		setTabWidthDropdown( items.rxTabWidth);
        tabWidthOnChange(null);

        document.getElementById('rxEnableOnReceiveError').checked = items.rxEnableOnReceiveError;
        document.getElementById('rxEnableShowReceiveError').checked = items.rxEnableShowReceiveError;

        document.getElementById('rxShowTimestamp').checked = rxShowTimestamp = items.rxShowTimestamp;// = items.rxShowTimestamp;
        document.getElementById('rxEnableShowHWLines').checked = rxEnableShowHWLines = items.rxEnableShowHWLines;

        rxTimestampArmed = rxShowTimestamp;   // only arm if enabled.
        // tx history saving
        setTxHistorySaving( items.txHistorySaving, true);
        //
        // start or stop the tx repeat mode as needed.
        txRepeatMode ? startTxRepeat() : stopTxRepeat();
		setAutoPortReopen( items.autoPortReopen);
    });
}
function setRepeatMode(val)
{
    txRepeatMode = val;
}
function getRepeatMode()
{
    return txRepeatMode;
}

// save the txHistorySaving flag to local storage
function setTxHistorySaving(val, restore)
{
    txHistorySaving = val;
    console.log(MyFileName + ': setTxHistorySaving: '+txHistorySaving);

    chrome.storage.local.set({txHistorySaving: txHistorySaving}, function () {
        // when restoring and it is enabled, read the saved history.
        if( restore == true && txHistorySaving == true) restoreTxHistory();
        // when not restoring and enabling, save current history to the storage.
        else if(restore == false && txHistorySaving == true) saveTxHistory();
        // when disabling, remove any local storage copy.
        else if(txHistorySaving == false) removeTxHistory();
    });

}
function getTxHistorySaving()
{
    return txHistorySaving;
}

// save the setAutoPortReopen flag to local storage
function setAutoPortReopen(val)
{
    autoPortReopen = val;
    console.log(MyFileName + ': setAutoPortReopen: '+autoPortReopen);

    chrome.storage.local.set({autoPortReopen: autoPortReopen}, function () {
    });

}
function getAutoPortReopen()
{
    return autoPortReopen;
}
// ------------------------------------
// dropdown checkbox code below
var rxCheckList1 = document.getElementById('rxDisplayList1');
var rxCheckList2 = document.getElementById('rxDisplayList2');

var rxItems1 = document.getElementById('rxItems1');
var rxFrontlayer1 = document.getElementById('rxFrontlayer1');

var rxItems2 = document.getElementById('rxItems2');
var rxFrontlayer2 = document.getElementById('rxFrontlayer2');

rxCheckList1.getElementsByClassName('anchor')[0].onclick = function(evt) {
  if (rxFrontlayer1.style.visibility === 'visible') { // collapse list.
    //items.classList.remove('visible');
    rxItems1.style.display = "none";
    hideRxFrontLayer(rxFrontlayer1);
  } else { // expand list
    //items.classList.add('visible');
    rxItems1.style.display = "block";
    showRxFrontLayer(rxFrontlayer1);
  }
};
rxCheckList2.getElementsByClassName('anchor')[0].onclick = function(evt) {
  if (rxFrontlayer2.style.visibility === 'visible') { // collapse list.
    //items.classList.remove('visible');
    rxItems2.style.display = "none";
    hideRxFrontLayer(rxFrontlayer2);
  } else { // expand list
    //items.classList.add('visible');
    rxItems2.style.display = "block";
    showRxFrontLayer(rxFrontlayer2);
  }
};
rxItems1.onblur = function(evt) {
  rxItems1.classList.remove('visible');
};
rxItems2.onblur = function(evt) {
  rxItems2.classList.remove('visible');
};
function showRxFrontLayer(layer) {
  layer.style.visibility = 'visible';
}
function hideRxFrontLayer(layer) {
  layer.style.visibility = 'hidden';
}

// -----------------------------------------------------------------------------
//		FILE IO
// -----------------------------------------------------------------------------
// new file io methods based on code from here: https://github.com/ChrisJohnsen/StackOverflow-21896363/blob/so21896363/7/page.js
var fileHandler = function () {
    var _entry = null;
    this.openFile = function(filename, f_callback){
		// filename can be a filter, such as "*.dat" or "*"
        // caller's f_callback is passed the file object read in.
        chrome.fileSystem.chooseEntry({ type: 'openFile', suggestedName: filename },
        function(fileEntry) {
            if (chrome.runtime.lastError) {
                console.error(MyFileName + ' fileSystem.chooseEntry(): chrome API lastError:', chrome.runtime.lastError.message);
                f_callback && f_callback(null);
                return;
            }
            console.log(MyFileName + " fileHandler.openFile(): fileEntry: ", fileEntry);
            _entry = fileEntry;
            readFileEntry(_entry, f_callback);
        });
    };

    // TODO switch to using this saveAs method
    this.saveAs = function(filename, source) {
        var config = {type: 'saveFile', suggestedName: filename};
        chrome.fileSystem.chooseEntry(config,
        function(entry) {
            if (chrome.runtime.lastError) {
                console.error(MyFileName + ' fileHandler.saveAs.chooseEntry(): chrome API lastError:', chrome.runtime.lastError.message);
                return;
            }
            chrome.fileSystem.getWritableEntry(entry, function(entry) {
                if (chrome.runtime.lastError) {
                    console.error(MyFileName + ' fileHandler.saveAs.getWritableEntry(): chrome API lastError:', chrome.runtime.lastError.message);
                    return;
                }
                entry.getFile(filename, {create:true}, function(entry) {
                    entry.createWriter(function(writer) {
                        writer.onwrite = function() {
                            writer.onwrite = null;
                            writer.truncate(writer.position);
                        };
                        writer.onwriteend = function (e) {
                            if (chrome.runtime.lastError) {
                                console.error(MyFileName + ' saveTextToFile: chrome API lastError:', chrome.runtime.lastError.message);
                            }else{
                                setStatus('File Save Complete. size: ' + blob.size, 'green');
                                console.log(MyFileName + ' saveTextToFile() callback. size:' + blob.size + ' ProgressEvent:', e);
                            }
                        };
                        writer.write(new Blob([source], {type: 'text/plain'}));
                    });
                });
            });
        });
    };

    var readFileEntry = function(readableEntry, callback) {
        if (!readableEntry) {
            setStatus('Nothing Read!', 'red');
            console.log(MyFileName +  ' readFileEntry(): Nothing selected.');
            return;
        }
        readableEntry.file(function(file) {
            console.log(MyFileName + ' readable.file()\n  file: '+file.path + '\n  size: ' +file.size);
            var reader = new FileReader();
            reader.onerror = errorHandler;
            reader.onload = function(e) {
                callback(e);// send the text blob to the callback; in e.target.result
            };
            reader.readAsText(file);
        });
    };
};
/* ***************************************************************************** */
/* Code below this line:
 * File IO based on Google example. API info: https://developer.chrome.com/apps/fileSystem
 Copyright 2012 Google Inc.

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.

 Author: Eric Bidelman (ericbidelman@chromium.org)
 Updated: Joe Marini (joemarini@google.com)
 Modified for this app: Mark Giebler
 */

// file and dir buttons ??
var saveFileButton = document.querySelector('#rxSaveAs');
var sendFileButton = document.querySelector('#tXSendFile');
var textarea = rxDataElement; //document.querySelector('rxdata');

function errorHandler(e) {
    console.error(e);
}

function waitForIO(writer, callback) {
    // set a watchdog to avoid eventual locking:
    var start = Date.now();
    // wait for a 10 seconds
    var reentrant = function () {
        if (writer.readyState === writer.WRITING && Date.now() - start < 10000) {
            setTimeout(reentrant, 200);
            return;
        }
        if (writer.readyState === writer.WRITING) {
            console.error("Write operation taking too long, aborting!" +
                    " (current writer readyState is " + writer.readyState + ")");
            writer.abort();
        } else {
            console.log('waitForIO: readyState:' + writer.readyState);
            callback();
        }
    };
    setTimeout(reentrant, 200);
}

function writeFileEntry(writableEntry, opt_blob, callback) {
    if (!writableEntry) {
        setStatus('Nothing saved!', 'red');
        console.log('writeFileEntry(): Nothing selected.');
        return;
    }
    writableEntry.createWriter(function (writer) {
        writer.onerror = errorHandler;
        writer.onwriteend = callback;
        console.log('createWriter called...');
        // If we have data, write it to the file.
        if (opt_blob) {
            console.log('have blob, size: ', opt_blob.size);
            writer.truncate(opt_blob.size);
            waitForIO(writer, function () {
                writer.seek(0);
                writer.write(opt_blob);
            });
        } else {
            console.error('Error: writeFileEntry: no blob.');
        }
    }, errorHandler);
}

var chosenEntrySave = null; // an Entry returned by a chrome file API callback.
saveFileButton.addEventListener('click', function (e) {
    var aName = 'rxbuffer.txt';
    if (chosenEntrySave) { // override with last used filename.
        aName = chosenEntrySave.name;
    }
    console.log('RxSaveAs clicked. Name: ' + aName );
    saveTextToFile( aName, [textarea.textContent]);
});

// saveTextToFile() also used in options.js
function saveTextToFile( filename, textblob){
    console.log(MyFileName + ' saveTextToFile() name: ' + filename);
    var config = {type: 'saveFile', suggestedName: filename};
    chrome.fileSystem.chooseEntry(config, function (writableEntry) {
        if (chrome.runtime.lastError) {
            console.error(MyFileName + ' saveTextToFile: chrome API lastError:', chrome.runtime.lastError.message);
        }else{
            var blob = new Blob([textblob], {type: 'text/plain'});
            chosenEntrySave = writableEntry;
			// use local storage to retain access to this file
			chrome.storage.local.set({'chosenEntrySave': chrome.fileSystem.retainEntry(writableEntry)});

            console.log(MyFileName + ' saveTextToFile() Choosen name: ' + chosenEntrySave.name);
            writeFileEntry(writableEntry, blob, function (e) {
                if (chrome.runtime.lastError) {
                    console.error(MyFileName + ' saveTextToFile: chrome API lastError:', chrome.runtime.lastError.message);
                }else{
                    setStatus('File Save Complete. size: ' + blob.size, 'green');
                    console.log(MyFileName + ' saveTextToFile() callback. size:' + blob.size + ' ProgressEvent:', e);
                }
            });
        }
    });
}

// --------------------------------------
//  Read File for sending out serial port
// -------------------------------------
function readAsText(fileEntry, callback) {
  fileEntry.file(function(file) {
    var reader = new FileReader();

    reader.onerror = console.error;
    reader.onload = function(e) {
      callback(e.target.result);
    };

    reader.readAsText(file);
  });
}

var chosenEntrySend = null; // save Send File Entry returned by a chrome file API callback.
// for files, read the text content into the textarea
function loadFileEntry(_chosenEntry) {
  chosenEntrySend = _chosenEntry;
  console.log(MyFileName + ' loadFileEntry() name: ' + chosenEntrySend.name);
  chosenEntrySend.file(function(file) {
		_entry = chosenEntrySend;
		readAsText(_entry, function(result) {
			if ( logDebugLevel >= 3)
				console.log(MyFileName + ' loadFileEntry() -> readAsText() result: ' + result);
			// send the file.
			sendTextByLine(result);
		});
  });
}

// read file in when SendFile button clicked.
sendFileButton.addEventListener('click', function(e) {
  var accepts = [{
    mimeTypes: ['text/*'],
    extensions: ['srec', 'hex', 'ihex', 'txt', 's', '*', 'csv']
  }];
  console.log('TxSendFile clicked.' );
  chrome.fileSystem.chooseEntry({type: 'openFile'}, function(theEntry) {
	if (chrome.runtime.lastError) {
		console.error(MyFileName + ' sendFileButton(): chrome API lastError:', chrome.runtime.lastError.message);
		setStatus('Send File: '+chrome.runtime.lastError.message, 'red');
	}else{
		if (!theEntry) {
			setStatus('No File Selected!', 'red');
			console.log('sendFileButton(): Nothing selected.');
		 return;
		}
		console.log('sendFileButton(): Selected File: '+ theEntry.name);
		setStatus('Send File: '+ theEntry.name, 'blue');
		// use local storage to retain access to this file
		chrome.storage.local.set({'chosenFileSend': chrome.fileSystem.retainEntry(theEntry)});
		loadFileEntry(theEntry);
	}
  });
});

