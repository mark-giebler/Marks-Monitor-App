/*
 Copyright 2013-2019 Mark Giebler

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Author: Mark Giebler

 */
window.opener.console.log("about.html opened. logLvl: "+window.opener.getLogLevel());
var logDirection=0;  // default to decrease log level on entry.
document.getElementById('myIconLog').addEventListener('click', function (e) {
    if( (window.opener.getLogLevel() % window.opener.getMaxLogLevel()) === 0) logDirection ^= 1;
    window.opener.console.log("about.js: logDir: "+logDirection);
    if( logDirection === 1)
        window.opener.increaseLogLevel();
    else
        window.opener.decreaseLogLevel();
  document.getElementById("demo").innerHTML = "Log Level bumped to: "+window.opener.getLogLevel();

 });

