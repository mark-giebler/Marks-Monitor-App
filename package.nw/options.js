/*
 Copyright 2013-2019 Mark Giebler

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Author: Mark Giebler

 */
var repeatDelaySelection= [0.1,0.2,0.3,0.5,0.8,1,2,3,4,5,6,7,8,9,10,15,20,30,45,60,180,600,3600];
// define delays for file sending
var txLineDelaySelection= [1,3,6,10,25,50,75,100,200,350,500,750,1000,3500,5000,7500,9999];
// build_Dropdown
//  dropElementId - id of exisitng element in DOM to be filled in.
//  dropSelection - array of string values for the selections
//      array format optionally passed in on dropSelections
//          { displayName: '1MB',   data:1000000 }  or
//          { portUniqueName: ...:}
//  dropNames - optional - array of names to display for the selections.
function build_Dropdown(dropElementId, dropSelections, dropNames){
  var theDropdown;
  if( (theDropdown = document.getElementById(dropElementId)))
  {
    var anOption = document.createElement('option');
    window.opener.console.log("options.js: build_Dropdown(" + dropElementId + "): start...",dropSelections);
    var index=0;
    if(dropSelections)
    for(var aSelection of dropSelections){
      var anOption = document.createElement('option');
      if(aSelection.data){
        anOption.value = aSelection.data;
        anOption.textContent = aSelection.displayName;
      }else if(aSelection.portUniqueName){
        anOption.value = aSelection.portUniqueName;
        if(aSelection.portUniqueName.match(/None/)){
            anOption.textContent = ' Default Settings';
            }else
            {
                anOption.textContent = aSelection.portUniqueName;
            }
      }else{ // just plain array.
        anOption.value = aSelection;
        if(!dropNames){
          anOption.textContent = aSelection;
        }else{
          anOption.textContent = dropNames[index];
        }
      }
      index++;
      theDropdown.appendChild(anOption);
      window.opener.console.log("options.js: build_Dropdown(" + dropElementId + "): adding: ", aSelection);
    };
    window.opener.console.log("options.js: build_Dropdown(" + dropElementId + "): done.");
  }else
  {
      window.opener.console.error("options.js: build_Dropdown(" + dropElementId + "): does not exist.");
  }
}

// sort a drop down list
function sortDropdownList(ddId){
    var ddl=document.getElementById(ddId);
    window.opener.console.log("options.js: sortDropdownList() in:",ddl);
    var options = [].slice.apply(ddl.options, [0]);
    window.opener.console.log("options.js: sortDropdownList() slice:",options);
    ddl.innerHTML = "";
    var sorted = options.sort(function(a,b){
       if(a.textContent < b.textContent) return -1;
        if(a.textContent > b.textContent) return 1;
        return 0;
    });
    window.opener.console.log("options.js: sortDropdownList() sort:",options);
    for(var i = 0; i < sorted.length; i++){
      ddl.options.add(sorted[i]);
    }
    window.opener.console.log("options.js: sortDropdownList() out:",ddl);
}
var selectedSerialPort='';
var portOptions; // current open ports settings.
var portSettingsArrayOptions;// array of each ports settings.
var customPorts;
var customTxButtons;

// deleteSerialPortSettings( name )
// will filter the requested name out of the serialPortSettingArray.
//
function deleteSerialPortSetting( ){
    window.opener.console.log('options.js: deleteSerialPortSetting: start. Port: ',selectedSerialPort,portSettingsArrayOptions);
    if(!selectedSerialPort.match(/None/)){
        // don't delete the default port (None...)
        portSettingsArrayOptions = portSettingsArrayOptions.filter(function(portSet) {
          return( portSet.portUniqueName !== selectedSerialPort );
        });
        window.opener.console.log('options.js: deleteSerialPortSetting: : ',portSettingsArrayOptions);
        // now removed deleted item from dropdown list
        var theElement = document.getElementById('serialPort');

        for (i=0;i<theElement.length;  i++) {
           if (theElement.options[i].value === selectedSerialPort) {
             theElement.remove(i);
           }
        }
    }
    // select Default port after delete.
    selectedSerialPort = document.getElementById('serialPort').value = portSettingsArrayOptions[0].portUniqueName;
    window.opener.console.log('options.js: deleteSerialPortSetting: done.');
}

// getCustomPorts
// update the customePorts object with the data entered in the web page.
function getCustomPorts()
{
    window.opener.console.log("options.js: getCustomPorts() started. ");
    var id=1;
    var elm=0;
    customPorts.length = 0;     // remove all ports from the array
    {
        var pathId='cpath'+id;
        var descId='cdesc'+id;
        var pathElm;
        while( pathElm = document.getElementById( pathId ) ){
            if(pathElm.value != ""){
                customPorts.push( {path: pathElm.value, displayName: document.getElementById(descId).value} );
                elm++;
            }
            window.opener.console.log('options.js: getCustomPorts() id: '+id+' port: ', customPorts[elm-1]);
            id++;
            pathId='cpath'+id;
            descId='cdesc'+id;
        }
    }
    window.opener.console.log( "options.js: getCustomPorts() length: "+customPorts.length );
}

function build_customPorts(){
    var id=1;
    if(customPorts)
        for(var port of customPorts){
            if(port){
                var pathId='cpath'+id;
                var descId='cdesc'+id;
                var pathElm = document.getElementById(pathId);
                window.opener.console.log('options.js: build_customPaths: '+id+' port: ',port);
                if(!pathElm){
                    addCustomPortRow();
                }
                document.getElementById(pathId).value = port.path;
                document.getElementById(descId).value = port.displayName;
            }
            id++;
        }
}
function addCustomPortRow() {
    window.opener.console.log("options.js: addCustomPortRow(): ");
    var objDiv = document.getElementById("optGroupCP");
    var count = objDiv.getElementsByTagName('div').length;
    count++;

    var div = document.createElement('div');
    div.className = 'row';
    div.innerHTML = count+'. Port path: <input id="cpath'+count+'" type="text" name="cpath'+count+'" \
class="dropdown-custom" style=\'width:14em\'> \
Description: [<input id="cdesc'+count+'" type="text" name="cdesc'+count+'" class="dropdown-custom" style=\'width:19em\'>]';

    objDiv.appendChild(div);
    objDiv.scrollTop = objDiv.scrollHeight;
    window.opener.console.log("options.js: addCustomPortRow(): added id#: "+count);
}

function build_customTxButtons(){
    var id=1;
    window.opener.console.log('options.js: build_customTxButtons: ',customTxButtons);
    if( customTxButtons )
        for(var txBtn of customTxButtons ){
            if(txBtn){
                var nameId='cbname'+id;
                var dataId='cbstring'+id;
                var pathElm = document.getElementById(nameId);
                if(!pathElm){
                    addCustomButtonRow();
                }
                window.opener.console.log('options.js: build_customTxButtons: '+id+' txBtn: ',txBtn);
                document.getElementById(nameId).value = txBtn.displayName;
                document.getElementById(dataId).value = txBtn.data;
            }
            id++;
        }
}
var maxQuickButtons=15; // make sure you enter in multiples of 5.
function addCustomButtonRow() {
    window.opener.console.log("options.js: addCustomButtonRow(): ");
    var objDiv = document.getElementById("optGroupBtn");
    var count = objDiv.getElementsByTagName('div').length;
    count++;
    if(count <= maxQuickButtons){
        var div = document.createElement('div');
        div.className = 'row';
        div.innerHTML = 'Button '+count+' Name: <input id="cbname'+count+'" type="text" name="cbname'+count+'" \
style=\'width:8em\'> String to send: <input id="cbstring'+count+'" type="text" name="cbstring'+count+'" style=\'width:21em\'>';
        objDiv.appendChild(div);
        objDiv.scrollTop = objDiv.scrollHeight;
        window.opener.console.log("options.js: addCustomButtonRow(): added id#: "+count);
    }else
    {
        window.opener.console.log("options.js: addCustomButtonRow(): at Maximum buttons #: "+(count-1));
        var status = document.getElementById('options_status');
        status.textContent = 'Maximum buttons reached.  ';
        setTimeout(function() {
            status.textContent = ''; // hide message after timeout.
        }, 1300);
    }
}
// getTxButtons
// update the customTxButtons object with data entered in the web page
// any empty buttons found after the first 5 are removed. (TODO: might want to change this)
function getTxButtons()
{
    window.opener.console.log("options.js: getTxButtons() started. ",customTxButtons);
    var id=1;
    var elm=0;
    customTxButtons.length = 0;     // remove all ports from the array
    {
        var nameId='cbname'+id;
        var dataId='cbstring'+id;
        var nameElm;
        while( (nameElm = document.getElementById( nameId )) ){
            if(nameElm.value != ""){
                customTxButtons.push( {displayName: nameElm.value, data: document.getElementById(dataId).value} );
                elm++;
            }else if(id < 6){
                // must have at least 5 buttons of data even if empty.
                customTxButtons.push( {displayName: "", data: ""} );
                elm++;
            }
            window.opener.console.log('options.js: getTxButtons() id: '+id+' txBtn: '+(elm-1)+' ', customTxButtons[elm-1]);
            id++;
            dataId='cbstring'+id;
            nameId='cbname'+id;
        }
    }
    window.opener.console.log( "options.js: getTxButtons() array length: "+customTxButtons.length );
}
// when checkbox is toggled change the delay rate visibility
function repeatModeOnChange(element)
{
    window.opener.console.log("options.js: repeatModeOnChange() element.id: " + element.id);
    window.opener.console.log("options.js: repeatModeOnChange() element.checked: " + element.checked);
//	var status = document.getElementById('options_status');
//    status.textContent = 'Check Changed. '+element.checked;
    element.checked ? document.getElementById('repeatDelayDiv').style.visibility = 'visible' :document.getElementById('repeatDelayDiv').style.visibility = 'hidden' ;
}

// get repeat mode Delay from HTML element.
function getSelectRepeatDelay() {
  var delayDropdown = document.getElementById('repeatDelay');
  var selectedDelay = delayDropdown.options[delayDropdown.selectedIndex].value;
  window.opener.console.log("options.js: repeat selectedDelay: " + selectedDelay);
  return selectedDelay;
}

// get repeat mode Count from HTML element.
function getRepeatCount() {
  var repeatCount = document.getElementById('repeatCount').value;
  window.opener.console.log("options.js: repeatCount: " + repeatCount);
  return repeatCount;
}

// get tx line delay from HTML element. For file transfer usage.
function getSelectTxLineDelay() {
  var txDelayDropdown = document.getElementById('txLineDelay');
  var selectedDelay = txDelayDropdown.options[txDelayDropdown.selectedIndex].value;
  window.opener.console.log("options.js: TxLine selectedDelay: " + selectedDelay);
  return selectedDelay;
}

function get_options_object(){
    try{
    // return an object that contains all the data for storage ID:  optionsStorageIDs
        var serialRate = + document.getElementById('serialRate').value;
        var dataBits = document.getElementById('dataBits').value;
        var dataParity = document.getElementById('dataParity').value;
        var dataStopBits = document.getElementById('dataStopBits').value;
        var ctsFlowControl = (document.getElementById('ctsFlowControl').value == 'true');

        var repeatDelay = + getSelectRepeatDelay(); // + to force returned value to a number.
        var repeatCount = + getRepeatCount();
		
		var txLineDelay = + getSelectTxLineDelay();
		
        var maxRxBufferSz = document.getElementById('maxRxBuffer').value;
        getCustomPorts();
        getTxButtons();

        window.opener.console.log("options.js: get_options_object() Port to save: ["+selectedSerialPort+"]");
        if(portSettingsArrayOptions){
            var portSettingObject = portSettingsArrayOptions.find(d=>d.portUniqueName===selectedSerialPort);
            if(portSettingObject){
                // TODO: remove name after a few versions:  This is to fix a bug in data stored.
                portSettingObject.portSettings.name = selectedSerialPort;
                portSettingObject.portSettings.bitrate=serialRate;
                portSettingObject.portSettings.dataBits=dataBits;
                portSettingObject.portSettings.parityBit=dataParity;
                portSettingObject.portSettings.stopBits=dataStopBits;
                portSettingObject.portSettings.ctsFlowControl = ctsFlowControl;
            }
        }
        var storeItems = { //  must match optionsStorageIDs in storage.js
            serialRate: serialRate,
            dataBits: dataBits,
            dataParity: dataParity,
            dataStopBits: dataStopBits,
            repeatDelay: repeatDelay,
            repeatCount: repeatCount,
            ctsFlowControl: ctsFlowControl,
            customTxButtons: customTxButtons,
            customPorts: customPorts,
            maxRxBufferSz: maxRxBufferSz,
            portSettingsArray: portSettingsArrayOptions,
			txLineDelay: txLineDelay
        //    repeatMode: repeatMode
        };
        return storeItems;
    }catch(err){ window.opener.console.log('options.js: get_options_object(): catch error:',err); };
}

// Saves options to chrome.storage
function save_options() {
    try{
        window.opener.console.log("options.js: save_options() started... ==============================================");
        var status = document.getElementById('options_status');
        status.textContent = 'Wait...  ';
        var repeatMode = document.getElementById('repeatMode').checked;
        var storeItems = get_options_object();
        window.opener.setTxHistorySaving( document.getElementById('txHistorySaving').checked );
		window.opener.setAutoPortReopen( document.getElementById('autoPortReopen').checked );
        // Save port settings to currently open port
        window.opener.console.log("options.js: save_options() Items to save: ", storeItems);
        chrome.storage.local.set(storeItems, function() {
          // Update status to let user know options were saved.
          status.textContent = 'Options saved.  ';
              window.opener.console.log("options.js:   options saved to Chrome storage. Will update opener window.");
              window.opener.setRepeatMode(repeatMode);
              window.opener.updateFromStorage(true);    // update items on main page, including port list (true)
              //window.opener.refreshPortDropdown();  // this closes any open port.
          setTimeout(function() {
            status.textContent = ''; hide_options(); // close options window after timeout.
          }, 900);
        });
    }catch(err){ window.opener.console.log('options.js: save_options(): catch error:',err); };
}
// Restores select box and checkbox state using the preferences
// stored in chrome.storage.
function restore_options() {
    try{
    window.opener.console.log("options.js: restore_options() defaults: ",optionsStorageIDs);

    chrome.storage.local.get(optionsStorageIDs, function(items) {
        document.getElementById('repeatDelay').value = items.repeatDelay;
        document.getElementById('repeatCount').value = items.repeatCount;
        document.getElementById('repeatMode').checked = window.opener.getRepeatMode();
        document.getElementById('txHistorySaving').checked = window.opener.getTxHistorySaving();
		document.getElementById('autoPortReopen').checked = window.opener.getAutoPortReopen();
		document.getElementById('txLineDelay').value = items.txLineDelay;

        customPorts = items.customPorts;
        build_customPorts();
        customTxButtons = items.customTxButtons;
        build_customTxButtons();
        // set delay time visibility based on restored checkbox state.
        repeatModeOnChange(document.getElementById('repeatMode'));
        // buffer size:  We assume current value is one of the valid drop down selections.
        document.getElementById('maxRxBuffer').value = items.maxRxBufferSz;//window.opener.getMaxRxBufferLength();
        // handle unique port settings
        portSettingsArrayOptions = items.portSettingsArray;
        // get current open port's settings and set gui elements to match
        portOptions = window.opener.getPortOptions();
        window.opener.console.log('options.js: restore_options() portOptions: ',portOptions);
        document.getElementById('serialRate').value = portOptions.bitrate;
        document.getElementById('dataBits').value = portOptions.dataBits;
        document.getElementById('dataParity').value = portOptions.parityBit;
        document.getElementById('dataStopBits').value = portOptions.stopBits;
        document.getElementById('ctsFlowControl').value = portOptions.ctsFlowControl;
        // build port selection drop box:
        build_Dropdown('serialPort',portSettingsArrayOptions);
        sortDropdownList('serialPort');
        // select the current open port.
        document.getElementById('serialPort').value = portOptions.name;
        selectedSerialPort = portOptions.name;

        window.opener.console.log("options.js: restore_options() items: ",items);
        window.opener.console.log("options.js: restore_options() DONE.  Page ready to roll. =============================================");
  });
  }catch(err){ window.opener.console.log('options.js: restore_options(): catch error:',err); };
}

function hide_options() {
    window.opener.console.log("options.html closed.");
    window.close();
}

function export_options(){
    window.opener.console.log("options.html export_options().");
    // get all the settings into an object.
    var exportObjects = {} ;
    exportObjects['AppManifest']=chrome.runtime.getManifest();
    exportObjects['optionsStorageIDs'] = get_options_object();

    // tx history buffer
    exportObjects['txHistoryData'] = window.opener.makeTxHistorySaveObject();

    // main storage:
    chrome.storage.local.get( mainStorageIDs, function (mainitems) {
        exportObjects['mainStorageIDs'] = mainitems;
        // make JSON data
        var JSONexportBuffer = JSON.stringify(exportObjects);
        window.opener.console.log("JSONoptoins: ", JSONexportBuffer);
        // save the json to a file
        window.opener.saveTextToFile('app_parameters.dat', JSONexportBuffer);
    });
    // Todo: figure out how to bring options page to front.
    // close options page after save so it is not hidden behind main app.
    hide_options();
}
function set_Options_objects(optionsStorageIDs){
    try{
        var serialRate = + optionsStorageIDs.serialRate;
        var dataBits = optionsStorageIDs.dataBits;
        var dataParity = optionsStorageIDs.dataParity;
        var dataStopBits = optionsStorageIDs.dataStopBits;
        var ctsFlowControl = optionsStorageIDs.ctsFlowControl;

        var repeatDelay = + optionsStorageIDs.repeatDelay;
        var repeatCount = + optionsStorageIDs.repeatCount;

        var customTxButtons = optionsStorageIDs.customTxButtons;
        var customPorts = optionsStorageIDs.customPorts;
        var maxRxBufferSz = optionsStorageIDs.maxRxBufferSz;
        var portSettingsArrayOptions = optionsStorageIDs.portSettingsArray;

        var storeItems = { //  must match optionsStorageIDs in storage.js
            serialRate: serialRate,
            dataBits: dataBits,
            dataParity: dataParity,
            dataStopBits: dataStopBits,
            repeatDelay: repeatDelay,
            repeatCount: repeatCount,
            ctsFlowControl: ctsFlowControl,
            customTxButtons: customTxButtons,
            customPorts: customPorts,
            maxRxBufferSz: maxRxBufferSz,
            portSettingsArray: portSettingsArrayOptions
        };
        window.opener.console.log("options.js set_Options_objects(): storedItems: ",storeItems);
        // todo save to local storage and recall.
        chrome.storage.local.set(storeItems, function() {
          // Update status to let user know options were saved.
            status.textContent = 'Options imported.  ';
            window.opener.console.log("options.js:   options imported and saved to Chrome storage. Will update opener window.");
            window.opener.updateFromStorage(true);  // update items on main page, including port list (true)
            window.opener.refreshPortDropdown();	// this closes any open port.
            setTimeout(function() {
                status.textContent = ''; hide_options(); // close options window after timeout.
            }, 900);
        });
    }catch(err){ window.opener.console.log('options.js: set_Options_objects(): catch error:',err); };
}
function import_options(){
    window.opener.console.log("options.html import_options().");
    var fh = new window.opener.fileHandler();
    if(fh){
        fh.openFile("*.dat",function(dataBlob) {
            if(dataBlob){
                window.opener.console.log('opened: ', dataBlob);
                var jsonData = dataBlob.target.result;
                var newArr = JSON.parse(jsonData);
                window.opener.console.log('JSON parsed: ',newArr);
				// Todo: add dialog page to let user select which items to restore: txhistory, QuickButtons, Port settings, Misc.
                // update the object with the imported data
                window.opener.initTxHistory(newArr.txHistoryData);
                window.opener.saveTxHistory();  // save newly imported history to local storage.
                // save newArr.mainStorageIDs
                chrome.storage.local.set(newArr.mainStorageIDs, function() {
					// finally set the Options objects to the import data. (and force main window to read in mainStorageIDs)
                    set_Options_objects(newArr.optionsStorageIDs);
                });

                window.opener.setStatus('Import settings Completed sucessfully' , 'green');
            }else{
                window.opener.console.log("options.html import_options():  got NULL on open file call.");
                window.opener.setStatus(' Import settings CANCELLED ' , 'red');
				status.textContent = 'Import Cancelled.  ';
				setTimeout(function() {
					status.textContent = ''; hide_options(); // close options window after timeout.
				}, 900);
            }
        });
    }
}

window.opener.console.log("options.html opened.");
build_Dropdown('repeatDelay',repeatDelaySelection);
build_Dropdown('txLineDelay',txLineDelaySelection);
build_Dropdown('serialRate',serialRateSelection);
build_Dropdown('dataBits',dataBitsSelection, dataBitsSelectionName);
build_Dropdown('dataParity',dataParitySelection);
build_Dropdown('dataStopBits',dataStopBitsSelection, dataStopBitsSelectionName);
build_Dropdown('ctsFlowControl',ctsFlowControlSelection, ctsFlowControlSelectionName);
build_Dropdown('maxRxBuffer',maxBufferArray);

window.opener.console.log("options.html dropDowns populated.");
document.addEventListener('DOMContentLoaded', function(){window.opener.console.log('options.js: DOMContentLoaded EventListner has fired!'); restore_options();});
document.getElementById('options_save').addEventListener('click', save_options);
document.getElementById('options_cancel').addEventListener('click', hide_options);
document.getElementById('options_export').addEventListener('click', export_options);
document.getElementById('options_import').addEventListener('click', import_options);
document.getElementById('options_deleteSerial').addEventListener('click', deleteSerialPortSetting);
document.getElementById('addCustomPort').addEventListener('click', addCustomPortRow);
document.getElementById('addCustomButton').addEventListener('click', addCustomButtonRow);

document.getElementById('serialPort').onchange = function() {
        var portDropdown = document.getElementById('serialPort');
        selectedSerialPort = portDropdown.options[portDropdown.selectedIndex].value;
        window.opener.console.log('options.html  serialPort.onchange: '+selectedSerialPort);
        if(portSettingsArrayOptions){
            var portSettingObject = portSettingsArrayOptions.find(d=>d.portUniqueName===selectedSerialPort);
            if(portSettingObject){
                document.getElementById('serialRate').value = portSettingObject.portSettings.bitrate;
                document.getElementById('dataBits').value = portSettingObject.portSettings.dataBits;
                document.getElementById('dataParity').value = portSettingObject.portSettings.parityBit;
                document.getElementById('dataStopBits').value = portSettingObject.portSettings.stopBits;
                document.getElementById('ctsFlowControl').value = portSettingObject.portSettings.ctsFlowControl;
            }
        }
    };
document.getElementById('repeatMode').onchange= function(){ repeatModeOnChange(this);};
window.opener.console.log("options.html EventListners wired up and ready to roll.");

