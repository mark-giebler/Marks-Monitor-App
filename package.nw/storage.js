/*

 Copyright 2013-2019 Mark Giebler

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Author: Mark Giebler

 */
var txButtonArray=[
    {displayName: 'QBF test',data:'The Quick Brown Fox Jumped Over the Lazy Dogs Back! 1234567890.'},
    {displayName: '', data:''},
    {displayName: '', data:''},
    {displayName: '', data:''},
    {displayName: '', data:''}
];
var customPortArray=[
    {path: '/dev/ftdiRED',displayName:'Custom Port - Config in Options'},
    {path: '', displayName: ''},
    {path: '', displayName: ''},
    {path: '', displayName: ''},
    {path: '', displayName: ''}
];
// max buffer size settings
var maxBufferArray=[
    {displayName: '2.5MB', data:2500000},
    {displayName: '1MB',   data:1000000},
    {displayName: '500KB', data: 500000},
    {displayName: '250KB', data: 250000},
    {displayName: '100KB', data: 100000},
    {displayName: '50KB' , data:  50000},
    {displayName: '25KB' , data:  25000},
    {displayName: '10KB' , data:  10000},
    {displayName: '5KB'  , data:  5000},
    {displayName: '2.5KB', data:  2500}
];
var portSettings = { //Must match Chrome API port object.
    name: 'None (Close port)',
    bufferSize: 4096,
    bitrate: 57600,
    dataBits:"eight",
    parityBit:"no",
    stopBits:"one",
    ctsFlowControl:false
};
// port parameter setting array
//  Unique Port settings for each port that is on this local machine.
//  Objects created first time a port is opened.
var portSettingsArray=[
    {portUniqueName: 'Default Settings', portSettings: portSettings}
];

// for common storage related data in Options page.
var optionsStorageIDs = {
    serialRate: 57600,
    dataBits: 'eight',
    dataParity: 'no',
    dataStopBits: 'one',
    ctsFlowControl: false,
    repeatDelay: 4,
    repeatCount: 0,
	txLineDelay: 10,
    maxRxBufferSz: 100000,
    customTxButtons: txButtonArray,
    customPorts: customPortArray,
    portSettingsArray:[{portUniqueName: 'None (Close port)', portSettings: portSettings}]
};
// ...Selection values are what get sent to serial API.  ...SelectionName is what is displayed in GUI.
var serialRateSelection = [110, 300, 1200, 2400, 4800, 9600, 14400, 19200, 38400, 57600, 115200, 230400, 460800, 921600];
var dataBitsSelection = ['seven','eight'];
var dataBitsSelectionName = ['7','8'];
var dataParitySelection = ['no','odd','even'];
var dataStopBitsSelection = ['one','two'];
var dataStopBitsSelectionName = ['1','2'];  // used in gui display
var ctsFlowControlSelection = [true,false];
var ctsFlowControlSelectionName = ['CTS/RTS','None']; // used in gui display

var mainStorageIDs ={
    txAddCR:true,   // a check box
    txAddLF:true,   // a check box
    txAutoClear:false, // a check box
    txImmediate:false,
    txEcho:false,
    rxAutoScroll:true,
    rxAutoWrap:true,
    txQuickHide:false,
    rxShowCtrls:false,
    rxFilterNulls:true,
    rxEnableOnReceiveError:true,
    rxEnableShowReceiveError:false,
    rxShowTimestamp:false,
    rxEnableShowHWLines:true,
    txHistorySaving:true,
    rxShowCrtlHex:false,
    rxShowCtrlHex0:false,
    rxShowEOLrl:false,
    rxShowEOLesc:false,
    rxShowEOLhex:false,
    rxShowTab:false,
    rxShowBackspace:false,
    rxFilterBells:true,
	rxtxFontSize:'10px',
	rxGuiTheme:'ig',
	rxTabWidth:'4',
	autoPortReopen:false
} ;
