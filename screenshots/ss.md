# Screen shots
Here you will find some screenshots of my app in action.

* Example of main app window  
![Alt text](/screenshots/Ex1-main.png?raw=true "Main GUI")

* Drop down selection options on the main window  
![Alt text](/screenshots/Ex3-main-dropdowns.png?raw=true "Main GUI selections")

* Options page  - configuration/settings  
![Alt text](/screenshots/Ex2-options.png?raw=true "Options page")

* Each serial port device can have multiple configurations saved, each with its own bit rate, number of bits, etc...  
![Alt text](/screenshots/Ex4-options-multiconfig.png?raw=true "Multiple configs per port")  
You can create new configs using the **Custom port paths** area;  
click the plus sign to make a new config, 
fill in the device path (or COM# in Windows) and add a unique description.  After you save and re-enter 
the options page, you can select the new config in the port dropdown list to set the bit rate and other parameters.


