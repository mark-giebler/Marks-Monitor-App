# Mark's Serial Monitor -  A Chrome (and NW.js) App   

Author: Mark Giebler

https://gitlab.com/mark-giebler/Marks-Monitor-App

This is my chrome app.  It runs in Google's Chrome browser. Linux, Mac, or Windows.
It can also be run using [NW.js](https://nwjs.io/) - This is handy since Google 
will be removing chrome app support in 2018.  
The app communicates with serial devices over a PC's serial port using  
Chrome's (or NW.js) serial port API.  
Use it to communicate with Arduinos, or just about any serial device.

### The App provides:
* - a method to select which serial port to open.
* - options to set serial port settings (bit rate, # of bits, flow control, etc)
* - option to add custom serial device port paths (for Linux OS, to open Symlinks you created with udev rules)
* - option to assign canned tx strings to Quick Send buttons
* - history buffer for last 64 Transmit strings  (use up and down arrows in the tx input box to scroll through)
* - options to automatically add end-of-line character(s) to tx strings
* - Auto repeat last command on a configurable timer interval
* - Display input control lines (CTS, etc)
* - Ability to change output control lines (RTS and DTR)
* - Ability to save RX data buffer to a file.
* - Ability to enable/disable auto scrolling of the Rx data window.
* - Ability to display control characters in the Rx data.
* - Ability to filter out null chars from Rx data.
* - Most settings are automatically persisted using Chrome API local storage.
* - Settings can be exported and imported to/from files
* - And more....

## Screen shots
* Example of main app window 

![Alt text](/screenshots/Ex1-main.png?raw=true "Example 1 Main")
* [see more here](/screenshots/ss.md) 

## Installing the Chrome App in Google Chrome browser

### Method 1: Unpacked:
	Go to chrome://extensions and  enable the check box "Developer Mode" 
	then drag the app's unpacked directory (package.nw) over to the chrome browser's chrome://extensions tab, follow the install prompts.

### Method 2: Packed:
	Go to chrome://extensions and enable the check box "Developer Mode" 
	Then drag the Marks-Monitor-App.crx packed file over to the chrome browser's chrome://extensions tab, follow the install prompts.
	

## Using with NW.js instead of the Chrome browser

As an alternative to using Google Chrome to run this app, you can use **NW.js**.
This will be the only way to run this app after google removes chrome API support
from the Chrome browser in 2018.

### Running with NW.js Instructions:
1. download and extract NW.js to a convenient location on your device. Find it here: [NW.js](https://nwjs.io/)
2. Copy my app's directory **package.nw** to the NW.js binary directory
3. Run the nw binary and it will launch the app automatically.

## Feature Highlights
Binary data can be sent by entering hex or binary escape sequences in the tx input box.
* First make sure **Send Immediate** is UN-checked. Then enter the escape sequence in the tx input box, and click **send** or hit the **enter** key.
* Example hex sequences: **\0x8A** or **\x8A**
* Example binary sequence: **\b10001010**
* Printable characters and escape sequences can be combined:
	**Hello W\b01101111rld\0x20\0x0A\b11101001**
* Also supports **\r** and **\n** escape sequences for CR and LF respectively.

To send control characters normally used for editing (Ctrl-A, Crtl-C, Crtl-V), the shift key must be added. 
* Make sure **Send Immediate** is Checked
* Then to send Control C use the key combo  **Shift-Ctrl-C**
 
ANSI up/down/left/right arrow key sequences can be sent using the Shift key in combination with the arrow keys:
* Make sure **Send Immediate** is Checked
* click in the tx input box to make it active, then for example, **Shift-UpArrow** will send the ANSI escape sequence for Up: **\0x1B[A**

Deleting line items from the history buffer:
* Click in tx input box, then use up/down arrow keys to select a saved history item, then Ctrl-Del to delete the item from history.

### Windows OS annoyances
I've only tested a little bit on Windows, but one annoying thing I've observed 
is that the serial API does not behave well with FTDI USB to Serial adapters
(or rather the FTDI driver does not behave well...)  

Problems with FTDI devices:
1. If the bit rate is not correct, and a framing error event is thrown, 
the serial API will close the port connection to the FTDI device automatically.
2. If a high bit rate device starts to send data immediately after the port is opened, garbage
characters show up in the receive buffer that the device did not even send. This also happens with
other terminal program on Windows, so seems to be an FTDI related issue.  I've confirmed with 
my Rigol oscilloscope that the device is not sending the characters the receive buffer contains.

The above issues only happen with FTDI devices on Windows.  On Linux, FTDI devices play very well.
Other USB to serial devices I have tested on Windows do NOT have this strange behavior; 
Such as PL2302, CP2104, and CH340.  It seems only FTDI devices do this.

### Google API info

* [Serial API](http://developer.chrome.com/trunk/apps/app.hardware.html#serial)

### License Stuff
 Copyright 2013-2017 Mark Giebler
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 
 Author: Mark Giebler
